﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CheckInOut : ContentPage
    {
        string setempid, empname, setcompid, get_id, dateformat, add_get, eventid, JsonResult, result, latitude, longitude, getnotes, a1, b1, addressSelected;
        string[] tokens;
        public int n = 20;
        Boolean getval;
        HttpClient client;
       
        string ItemID = "";

        // Normal button height
        private const int BUTTON_HEIGHT = 66;
        private const int BUTTON_HEIGHT_WP = 144;
        public CheckInOut()
        {
            InitializeComponent();

            Title = "CheckIn-CheckOut";

            indicator1.IsVisible = true;
            indicator1.IsRunning = true;
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];

            else
            {

            }
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            else
            {

            }
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
            else
            {

            }
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            else
            {

            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            else
            {

            }
            Add.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
            Delete.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
           
            Edit.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
            check_connection();
            Loadaddress1();
            OngetLocation();
            time.Text = (string)App.Current.Properties["todaydate"];
          
            date.Text = DateTime.Now.ToString("HH:mm tt");
          
            getval = Helpers.Settings.Displaybuttonstatus;
          

            if (getval == true)
            {
                btn_check.Text = "Check-out";

            }

            else
            {

                btn_check.Text = "Check-in";

            }

            Java.Lang.Thread.Sleep(2000);

            checklocconnect();

        }
        private void onselecteditemchanged(object sender, EventArgs e)
        {
            try
            {
                Delete.IsVisible = true;
                Delete.IsEnabled = true;
                // slash.IsVisible = true;
                //  slash.IsEnabled = true;
                Edit.IsVisible = true;
                Edit.IsEnabled = true;
                if (mypicker.Items.Count == 0)
                {
                   // mypicker.IsEnabled = false;

                }else { 
                addressSelected = mypicker.Items[mypicker.SelectedIndex];

                if (addressSelected == add_get)
                {
                    Delete.IsVisible = false;
                    //slash.IsVisible = false;
                    Edit.IsVisible = false;

                }
                else
                {
                    int i = mypicker.SelectedIndex;
                    if (i != -1)
                    {
                        string Addressselected = (sender as Picker).SelectedItem.ToString();
                        for (int i1 = 0; i1 < tokens.Length; i1++)
                        {
                            string a = tokens[i1];
                            string b = a.Trim('"').Trim('"');
                            string[] id = b.Split('-');
                            string Add_id = id[0];
                            string Add_name = id[1];
                            if (Add_name == Addressselected)
                            {
                                ItemID = id[0];
                            }
                        }
                    }
                    else
                    {
                        mypicker.IsEnabled = false;
                        Delete.IsVisible = false;
                        Edit.IsVisible = false;
                    }
                }
            }

            }
            catch (Exception ex) { }
        }

        private async void OnaddClicked_Page(object sender, EventArgs e)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                Add.IsVisible = true;
                EnteredName.Text = null;
                overlay_checkin.IsVisible = true;
                title.Text = "  Add an Address";
                EnteredName.Placeholder = "Please Add an Address";
                btn_addedit.Text = "Add";
                btn_addedit.IsEnabled = false;

                if (EnteredName.Text != "" && EnteredName != null)
                {
                    btn_addedit.IsEnabled = true;
                }
                else
                {
                    btn_addedit.IsEnabled = false;
                }
            }
            else
            {

            }
        }

        private async void onAddEdit(object sender, EventArgs e)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                btn_addedit.IsEnabled = false;
                var get_string = EnteredName.Text;
                if (string.IsNullOrEmpty(get_string))
                {
                    await DisplayAlert("Oh Snap!", "Please enter some valid Address", "Ok");
                    btn_addedit.IsEnabled = true;
                }
                else
                {
                    get_string = UppercaseFirst(get_string);
                    get_string = get_string.Substring(0, 1) + get_string.Substring(1).ToLower();

                    if (btn_addedit.Text == "Add")
                    {


                        if (string.IsNullOrEmpty(EnteredName.Text) || string.IsNullOrWhiteSpace(EnteredName.Text))
                        {

                        }
                        else
                        {
                            await AddAdress();
                            mypicker.Items.Clear();
                            await Loadaddress1();
                            if (string.IsNullOrEmpty(add_get))
                            {
                                return;
                            }
                            else
                            {
                                mypicker.Items.Add(add_get);
                            }
                        }



                    }
                    if (btn_addedit.Text == "Edit")
                    {
                        await updateAddress(get_string);
                        mypicker.Items.Clear();
                        await Loadaddress1();

                        if (string.IsNullOrEmpty(add_get))
                        {
                            return;
                        }
                        else
                        {
                            mypicker.Items.Add(add_get);
                        }
                    }
                }
            }
            else {
            }

        }


        public async Task updateAddress(string selectedaddress)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", ItemID));
            postData.Add(new KeyValuePair<string, string>("address", selectedaddress));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/update_address", content);
            string Jsonvalue = response.Content.ReadAsStringAsync().Result;
            if (Jsonvalue != null)
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
                btn_addedit.IsEnabled = true;
                overlay_checkin.IsVisible = false;
            }
            else
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
                btn_addedit.IsEnabled = true;
                overlay_checkin.IsVisible = false;
            }
        }
        static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
        private async void btn_check_send(object sender, EventArgs e)
        {
         bool connectioncheck= await check_connection();
            if (connectioncheck == true) {
                btn_check.IsEnabled = false;
                indicator1.IsVisible = true;
                indicator1.IsRunning = true;
                if (mypicker.SelectedIndex == -1)
                {
                    await DisplayAlert("Alert", "Please select any Address ", "Ok");
                    btn_check.IsEnabled = true;
                }
                else
                {
                    addressSelected = mypicker.Items[mypicker.SelectedIndex];
                    string btn_value = btn_check.Text.ToString();
                    if (btn_value.Equals("Check-in"))
                    {
                        checkin(latitude, longitude);
                    }
                    else if (btn_value.Equals("Check-out"))
                    {
                        geteventid();
                    }
                    else
                    {
                        btn_check.IsEnabled = true;
                       
                    }
                }
            }
            else
            {
                indicator1.IsVisible = false;
                indicator1.IsRunning = false;
            }
        }


        private async void geteventid()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_event_id, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Eventid>(result.ToString());
            eventid = rootobject.event_id;
            // await DisplayAlert("ok", eventid.ToString(), "ok");
            checkout(eventid, latitude, longitude);
        }
        private async void checkout(string eve, string lat, string longitude)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(setempid))
                {
                    setempid = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
                // await DisplayAlert("check out  companyid", setcompid, "okay");
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                postData.Add(new KeyValuePair<string, string>(Constants.address,addressSelected));
                postData.Add(new KeyValuePair<string, string>(Constants.notes, getnotes));
                postData.Add(new KeyValuePair<string, string>(Constants.latitude, lat));
                postData.Add(new KeyValuePair<string, string>(Constants.longitude, longitude));
                postData.Add(new KeyValuePair<string, string>("parent_id", eve));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Check_out, content);
                //to take the value direstly without the array 
                JsonResult = response.Content.ReadAsStringAsync().Result;
                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "Ok");
                Helpers.Settings.Displaybuttonstatus = false;
                Notes.Text = null;
                mypicker.SelectedIndex = -1;
                btn_check.IsEnabled = true;
                btn_check.Text = "Check-in";
            }
            catch (Exception e) {
            }
            finally
            {
                indicator1.IsVisible = false;
                indicator1.IsRunning = false;
            }
        }
        internal class Eventid
        {
            public string event_id { get; set; }
            public Eventid(string event_id)
            {
                this.event_id = event_id;
            }
        }
        private async void checkin(string lat, string longitude)
        {
            try { 
                var client = new System.Net.Http.HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();

                if (string.IsNullOrEmpty(setempid))
                {
                    setempid = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
                // await DisplayAlert("check in  companyid", setcompid, "okay");
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                postData.Add(new KeyValuePair<string, string>(Constants.address, addressSelected));
                postData.Add(new KeyValuePair<string, string>(Constants.notes, getnotes));
                postData.Add(new KeyValuePair<string, string>(Constants.latitude, lat));
                postData.Add(new KeyValuePair<string, string>(Constants.longitude, longitude));
                var content = new System.Net.Http.FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Check_in, content);
                //to take the value direstly without the array 
                JsonResult = response.Content.ReadAsStringAsync().Result;
                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "Ok");
                Helpers.Settings.Displaybuttonstatus = true;
                Notes.Text = null;
                mypicker.SelectedIndex = -1;
                btn_check.IsEnabled = true;
                //await DisplayAlert("Success!", Helpers.Settings.Displaybuttonstatus.ToString(), "ok");
                btn_check.Text = "Check-out";
            }
            catch(Exception e)
            {
             
            }
            finally
            {
                indicator1.IsVisible = false;
                indicator1.IsRunning = false;

            }
        }
        private const int BUTTON_HALF_HEIGHT = 44;

        public async  Task OngetLocation()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(1000));
            string.Format("Time: {0} \nLat: {1} \nLong: {2} \n Altitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \n Heading: {6} \n Speed: {7}",
              position.Timestamp, position.Latitude, position.Longitude,
              position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);
            latitude = position.Latitude.ToString();
            longitude = position.Longitude.ToString();
            if (locator.IsGeolocationEnabled)
            {
               
                Java.Lang.Thread.Sleep(1000);
                btn_check.IsEnabled = true;
                indicator1.IsVisible = false;
                indicator1.IsRunning = false;
                try
                {
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                    var postData = new List<KeyValuePair<string, string>>();
                    postData.Add(new KeyValuePair<string, string>("latitude", latitude));
                    postData.Add(new KeyValuePair<string, string>("longitude", longitude));
                  //  await DisplayAlert(latitude, longitude, "ok");
                    var content = new FormUrlEncodedContent(postData);
                    var response = await client.PostAsync(Constants.get_latlong_address, content);
                    var JsonResult = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                    string result = JsonResult.Trim(']').Trim('[');
                    var rootobject = JsonConvert.DeserializeObject<ItemAddress>(result.ToString());
                    string loadadd = rootobject.area_name;
                    if (loadadd.Equals("No"))
                    {
                        await DisplayAlert("Oh Snap!", "Address not found. Please Try again", "Ok");
                            indicator1.IsVisible = false;
                            indicator1.IsRunning = false;
                        }
                    else
                    {
                          
                                string[] tkn = loadadd.Split('-');
                            add_get = tkn[0] + ", " + tkn[1];
                            var action = await DisplayAlert("Would you like to add this location?", "Your Current Location is" + add_get, "Yes", "NO");
                        if (action == false)
                        {
                            return;
                           
                        }
                        else
                        {
                                mypicker.Items.Add(add_get);
                                mypicker.IsEnabled = true;
                               
                            }
                            indicator1.IsVisible = false;
                            indicator1.IsRunning = false;
                        }
                }
                else{
                        await DisplayAlert("Alert", response.ReasonPhrase.ToString(), "Ok");
                        indicator1.IsVisible = false;
                        indicator1.IsRunning = false;
                    }

                }
            catch (Exception exp) {
              await  DisplayAlert("Getting the location ","Please again press the Get Location because of "+exp.Message, "Ok");
                    indicator1.IsVisible = false;
                    indicator1.IsRunning = false;
                }
            finally
            {
                indicator1.IsVisible = false;
                indicator1.IsRunning = false;
              
            }
            }
            else
            {
                await DisplayAlert("Oh Snap!", "Please Turn ON the Location", "Ok");
            }
        }

        private const int BUTTON_HALF_HEIGHT_WP = 72;
        private const int BUTTON_WIDTH = 120;
        private const int BUTTON_WIDTH_WP = 144;
        private const int BUTTON_BORDER_WIDTH = 50;
        public Int32 BorderRadius { get; set; }
        public async Task AddAdress()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("emp_id", setempid));
            postData.Add(new KeyValuePair<string, string>("address", EnteredName.Text));
            postData.Add(new KeyValuePair<string, string>("company_id", setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/addaddress", content);
            string Jsonvalue = response.Content.ReadAsStringAsync().Result;
            if (Jsonvalue != null)
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
                btn_addedit.IsEnabled = true;
                overlay_checkin.IsVisible = false;
            }
            else
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
                btn_addedit.IsEnabled = true;
                overlay_checkin.IsVisible = false;
            }

        }

        public async Task Loadaddress1()
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>("emp_id", setempid));
                postData.Add(new KeyValuePair<string, string>("company_id", setcompid));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync("http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/getaddress", content);
                string Jsonvalue = response.Content.ReadAsStringAsync().Result;
                if (Jsonvalue == "No records Found")
                {

                    await DisplayAlert("Oh Snap!", "No Predefined Address, Please Add 1", "Ok");

                    mypicker.IsEnabled = false;
                }

                else
                {

                    mypicker.IsEnabled = true;
                    string result = Jsonvalue.Trim(']').Trim('[');
                    //await DisplayAlert("jsonResult", result, "ok");
                    tokens = result.Split(',');
                    for (int i = 0; i < tokens.Length; i++)
                    {
                        string a = tokens[i];
                        string b = a.Trim('"').Trim('"');
                        string[] id = b.Split('-');
                        //empname = id[1];
                        //Address.Items.Remove(empname);
                        //Address.Items.Remove(Addressselected);
                        //Address.Items.Add(empname);
                        string empname = id[1];
                        mypicker.Items.Add(empname);
                    }
                }
                mypicker.SelectedIndex = -1;
            }
            else {
                await DisplayAlert("Oh Snap!", "Address is not loaded Properly. Please check your Internet Connection", "Ok");
            }
           
        }
        public void checklocconnect()
        {
            var locator = CrossGeolocator.Current;
            if (!locator.IsGeolocationEnabled)
            {
                ////TODO: TEST CODE
                #region LOCATION PART
                ////TODO: FAKE:
                MessagingCenter.Send(new Message(true, n), string.Empty);            
                //return;
                #endregion
                btn_check.IsEnabled = false;
            }
            else
            {
            }
            if (locator.IsGeolocationEnabled)
            {
                Java.Lang.Thread.Sleep(1000);
                if (getval == true)
                {
                    btn_check.Text = "Check-out";
                }
                else
                {

                }
                if (getval == false)
                {

                    btn_check.Text = "Check-in";

                }
                else
                {

                }
            }
            else
            {

            }
        }

        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
        private async void ondeleteClicked(object sender, EventArgs e)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                Delete.IsEnabled = false;
                if (mypicker.SelectedIndex != -1)
                {
                    await DeleteAddress();
                    mypicker.Items.Clear();
                    await Loadaddress1();

                    if (string.IsNullOrEmpty(add_get))
                    {
                        return;
                    }
                    else
                    {
                        mypicker.Items.Add(add_get);
                        mypicker.IsEnabled = true;
                    }
                }
                else
                {
                    await DisplayAlert("Alert", "Please select any Address ", "Ok");


                    Delete.IsEnabled = true;

                }
            }
            else {
            }
        
    }
        public async Task DeleteAddress()
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();



            postData.Add(new KeyValuePair<string, string>("id", ItemID));

            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/delete_address", content);
            string Jsonvalue = response.Content.ReadAsStringAsync().Result;
            if (Jsonvalue != null)
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
            }
            else
            {
                await DisplayAlert("Success", Jsonvalue, "Ok");
            }


            Delete.IsEnabled = true;
        }




        private void into_clicked(object sender, EventArgs e)
        {
            overlay_checkin.IsVisible = false;
        }
        protected override bool OnBackButtonPressed()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


                bool getval = Helpers.Settings.DisplaySwitchstatus;
                if (locator.IsGeolocationEnabled && getval == true)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                    }
                    Helpers.Settings.DisplayExitstatus = false;
                    // return Helpers.Settings.DisplayExitstatus;
                }
                else
                {

                    Helpers.Settings.DisplayExitstatus = true;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                        if (await result)
                        {
                            Helpers.Settings.DisplayLoginstatus = true;
                            await Navigation.PushAsync(new Report_injury());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                        }
                        else
                        {
                            string empname;
                            if (App.Current.Properties.ContainsKey("employeename"))
                            {
                                empname = (string)App.Current.Properties["employeename"];
                                Helpers.Settings.DisplayLoginstatus = true;
                                var a = Helpers.Settings.DisplayLoginstatus;
                                // await  DisplayAlert("Empname and status", empname+a, "ok");
                            }

                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");

                            if (Device.OS == TargetPlatform.Android)

                                DependencyService.Get<IAndroidMethods>().CloseApp();

                            //    // base.OnBackButtonPressed();
                            //
                        }

                    });

                }
                base.OnBackButtonPressed();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("************************************************");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("************************************************");
            }
            System.Diagnostics.Debug.WriteLine("************************************************");
            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
            System.Diagnostics.Debug.WriteLine("************************************************");
            return true;
        }

        private async void OnupdateClicked_Page(object sender, EventArgs e)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                try
                {
                    if (mypicker.SelectedIndex != -1)
                    {


                        overlay_checkin.IsVisible = true;
                        string selectedAddress = mypicker.Items[mypicker.SelectedIndex];
                        EnteredName.Text = selectedAddress;
                        title.Text = "  Edit an Address";
                        EnteredName.Placeholder = "Please Edit a Address";
                        btn_addedit.Text = "Edit";
                    }

                    else
                    {
                        DisplayAlert("Alert", "Please select an Address ", "Ok");



                    }


                }
                catch (Exception exa)
                {
                    // DisplayAlert("OnUpdateClicked_Page", exa.Message, "okay");
                }

            }
            else { }
        }
        
    }
}