﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Emit;
using System.Text;
using TimeSheet_Inhouse.Views;
using Xamarin.Forms;
using TimeSheet_Inhouse.Helpers;
using TimeSheet_Inhouse.MenuItems;
using Plugin.Connectivity;
using ImageCircle.Forms.Plugin.Abstractions;
using Plugin.Geolocator.Abstractions;
using Java.Util;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse
{
    public class HomePage : ContentPage
    {
        private LoginDesign email;
        Image image;
        string logo;
        Label title;
        Button login;
     // string emp_name, accesscontrol, emp_id, emp_role, company_id, company_name, emp_email, setcompid,logged_value;
        private LoginDesignpassword password;
        public Int32 BorderRadius { get; set; }
        string setcompid;
        ActivityIndicator indicator;
        public HomePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            //getcompany_details();
            //getrole();
            check_connection();
            var s = new FormattedString();
           //// s.Spans.Add(new Span { Text = "Techiva  ", ForegroundColor = Color.FromHex("#141c77"), FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) });
            s.Spans.Add(new Span { Text = "Timesheet", ForegroundColor = Color.FromHex("#141c77"), FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) });
            //if (App.Current.Properties.ContainsKey("companyid"))
            //    setcompid = (string)App.Current.Properties["companyid"];
            title = new Label
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            
             title.FormattedText = s;

             indicator = new ActivityIndicator
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Color = Color.Black,
                IsVisible = false
            };

            image = new Image
            {
               Source= "ModelLogo.png",
                WidthRequest = 70,
                HeightRequest = 70

            };
            //for email connecting with Login design class
            email = new LoginDesign("emailout", "Email-Address");
           
            //for password
            password = new LoginDesignpassword("locklocked", "Password");
            //to set the password field as dot(...)
            Entry check = new Entry
            {
                IsPassword = true
            };
            password.Effects.Add(new ShowHidePassEffect());
             login = new Button
            {
                Text = "Login",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,
                BorderRadius = 20,


            };
            
            login.Clicked += async (object sender, EventArgs e) =>
            {
                try
                { 
                login.IsEnabled = false;
                    ////TODO: TEST CODE
                    //#region LOCATION PART

                    ////TODO: FAKE:
                    //MessagingCenter.Send(new Message(true), string.Empty);
                    //return;

                    //#endregion

                    //var networkConnection = DependencyService.Get<INetworkConnection>();
                    //networkConnection.CheckNetworkConnection();
                    //string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
                    //if (networkStatus.Equals("Connected"))
                    //{
                    bool getcheck = await check_connection();
                    if (getcheck == true)
                    {

                        var email_val = email.entry.Text;
                    var pass_val = password.entry.Text;
                         
                        if (string.IsNullOrEmpty(email_val) || string.IsNullOrEmpty(pass_val))
                        {
                            await DisplayAlert("Fields missing", "No email and/or password", "Ok");
                            login.IsEnabled = true;
                        }
                        else
                        {

                            indicator.IsRunning = true;
                            indicator.IsVisible = true;
                            var client = new HttpClient();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                            var postData = new List<KeyValuePair<string, string>>();
                          //  await DisplayAlert("pass_val",email_val, "ok");
                            //  postData.Add(new KeyValuePair<string, string>("name", "bkimbmn"));
                            postData.Add(new KeyValuePair<string, string>("email", email_val));
                            postData.Add(new KeyValuePair<string, string>("password", pass_val));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.Login_url, content);
                            string JsonResult = response.Content.ReadAsStringAsync().Result;

                          

                            //take the single element without the array can be used by this below line
                            //  var rootobject = JsonConvert.DeserializeObject<Person>(JsonResult);
                            //take  the single element with the array can be used by this below line
                            // this is to take the values from the array it trim the array holder from the json result
                            //string result = JsonResult.Trim('[').Trim(']');

                            // await DisplayAlert("back  Result", result, "ok");

                            //result = result.Trim('[');

                     
                            
                            var rootobject = JsonConvert.DeserializeObject<LoginUserData>(JsonResult);

                            string name_check = rootobject.name;
                            string emp_id_check = rootobject.emp_id;
                            string active = rootobject.is_active;
                           
                            System.Diagnostics.Debug.WriteLine("^^^^^^^^^^^^^^^^^^ Load_user_Details                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");







                            System.Diagnostics.Debug.WriteLine("empname: " + rootobject.name);

                            //emp_name = rootobject.name;



                            //emp_id = rootobject.emp_id;
                            System.Diagnostics.Debug.WriteLine("Employee_id: " + rootobject.emp_id);
                      


                          // company_id = rootobject.company_id;
                            System.Diagnostics.Debug.WriteLine("Company id: " + rootobject.company_id);

                           // company_name = rootobject.company_name;
                            System.Diagnostics.Debug.WriteLine("Company_name: " + rootobject.company_name);

                         
                            System.Diagnostics.Debug.WriteLine("Email: " + rootobject.company_name);


                           // emp_email = rootobject.email;
                            System.Diagnostics.Debug.WriteLine("Email: " + rootobject.email);

                            // accesscontrol = rootobject.permission;
                            System.Diagnostics.Debug.WriteLine("Emp role: " + rootobject.permission);
                            // if (string.IsNullOrEmpty(emp_name) || string.IsNullOrEmpty(emp_id))
                            if (string.IsNullOrEmpty(name_check) || string.IsNullOrEmpty(emp_id_check))
                            {
                                indicator.IsRunning = false;
                                indicator.IsVisible = false;
                                await DisplayAlert("Oh Snap!", "Invalid Credentials", "Ok");
                                email.entry.Text = null;
                                password.entry.Text = null;
                                login.IsEnabled = true;


                            }

                            else
                            {
                                if (active.Equals("0"))
                                {
                                    await DisplayAlert("Oh Snap!", " Your Account was Deactivated by the Administrator of Your Company", "Ok");
                                    indicator.IsRunning = false;
                                    indicator.IsVisible = false;
                                }

                                else
                                {

                                
                                try
                                {


                                    Helpers.Settings.DisplayEmployeename = rootobject.name;
                                    Helpers.Settings.Displayemployeeid = rootobject.emp_id;
                                    Helpers.Settings.Displaycompanyid = rootobject.company_id;

                                    Helpers.Settings.Displayemail = rootobject.email;

                                    Helpers.Settings.Displaycontrolrole = rootobject.permission;

                                    Helpers.Settings.DisplayCompanynameset = rootobject.company_name;
                                    Helpers.Settings.DisplayActiveEmailLoginstatus = rootobject.is_active;

                                    setcompid = Helpers.Settings.Displaycompanyid;
                                    indicator.IsRunning = false;
                                    indicator.IsVisible = false;
                                    System.Diagnostics.Debug.WriteLine("^^^^^^^^^^^^^^^^^^ -HomePage-                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                                    App.Current.Properties["employeename"] = rootobject.name;
                                    App.Current.Properties["employeeid"] = rootobject.emp_id;
                                    App.Current.Properties["companyname"] = rootobject.company_name;
                                    App.Current.Properties["companyid"] = rootobject.company_id;
                                    App.Current.Properties["email"] = rootobject.email;
                                    App.Current.Properties["controlaccess"] = rootobject.permission;
                                    string employee_name_pass = Helpers.Settings.DisplayEmployeename;
                                    string company_id_pass = Helpers.Settings.Displaycompanyid;
                                    string employee_id_pass = Helpers.Settings.Displayemployeeid;
                                    string email_pass = Helpers.Settings.Displayemail;
                                    string permission_pass = Helpers.Settings.Displaycontrolrole;
                                    string company_name = Helpers.Settings.DisplayCompanynameset;
                                    System.Diagnostics.Debug.WriteLine("GettingData:" + rootobject.name + rootobject.company_id + rootobject.emp_id + rootobject.email + rootobject.permission);
                                    System.Diagnostics.Debug.WriteLine("Company id" + rootobject.company_id);
                                    System.Diagnostics.Debug.WriteLine("Email" + rootobject.email);
                                    System.Diagnostics.Debug.WriteLine("empname" + rootobject.name);
                                    System.Diagnostics.Debug.WriteLine("Employee_id" + rootobject.emp_id);
                                    System.Diagnostics.Debug.WriteLine("Accesscontrol" + rootobject.permission);
                                    System.Diagnostics.Debug.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");



                                    MessagingCenter.Send<HomePage, string>(this, "companyid", company_id_pass);
                                    MessagingCenter.Send<HomePage, string>(this, "emailid", email_pass);
                                       
                                        //  await Navigation.PushAsync(new Enter_timesheet(emp_name, company_id, emp_id,emp_email, accesscontrol));
                                       
                                        await Navigation.PushAsync(new Enter_timesheet(rootobject.name, rootobject.company_id, rootobject.emp_id, rootobject.email, rootobject.permission));
                                      
                                        email.entry.Text = null;
                                    password.entry.Text = null;
                                        login.IsEnabled = true;
                                        //To send the values to the page. it will common we can access in any page through id eg:emplyeeid
                                        MessagingCenter.Send<HomePage, string>(this, "emplyeeid", employee_id_pass);
                                    MessagingCenter.Send<HomePage, string>(this, "emplyeename", employee_name_pass);
                                    MessagingCenter.Send<HomePage, string>(this, "companyid", company_id_pass);
                                       




                                        //await Navigation.PopAsync();
                                    }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine("TestCallStack: {0}", ex);


                                }
                            }
                        }
                    }
                }
                else
                {
                    await DisplayAlert("Whoops!", "Check your Connection", "Ok");
                }
            }
                catch(Exception ex)
                {

                }

            };

            Content = new ScrollView
            {
                Content = new StackLayout()
                {

                    HeightRequest = 700,
                    Children = {
              new StackLayout
           {
               Padding = new Thickness(20,90,20,0),
               Spacing = 20,
               Children = {
                      image,title,
               new Frame
               {
                   BackgroundColor=Color.FromHex("FFFFFF"),
                   OutlineColor =Color.White,
               HasShadow=true,

       Content=

       new StackLayout
          {
              Padding = new Thickness(15,50,15,0),
              Spacing = 20,
              Children = {

                   new StackLayout {
                       Children=
                       {
                        email, password, login
                       }

                   }



             }
                   }
               }, indicator

                      }


               }
       }
                }
            };



        }
       
        public async void getcompany_details()
        {
            var content = "";
            HttpClient client = new HttpClient();

            var RestURL = Constants.get_company_details;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Loadimg>(result.ToString());
            // to load the name from the database
            string companyname = rootobject.company_name;
            var s = new FormattedString();
            s.Spans.Add(new Span { Text = companyname + " ", ForegroundColor = Color.FromHex("#141c77"), FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) });
            s.Spans.Add(new Span { Text = "Timesheet", ForegroundColor = Color.FromHex("#2d8e3d"), FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) });
            title.FormattedText = s;
            //to load the image from the database
            logo = rootobject.logo;
            image.Source = ImageSource.FromUri(new Uri(logo));
           }
        protected override bool OnBackButtonPressed()
        {
            //MessagingCenter.Subscribe<Logedout, string>(this, "loggedout", (sender, arg) =>
            //{


            //    OnBackButtonPressed();

            //});

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to exiting the Application?", "Ok", "Cancel");
                if (await result)
                {
                    if (Device.OS == TargetPlatform.Android)

                    DependencyService.Get<IAndroidMethods>().CloseApp();
                }
                else
                {
                    return;                   
                }
            });
            //base.OnBackButtonPressed();
            return true;
        }
       
        public async void Loaddetails()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();  
                
            postData.Add(new KeyValuePair<string, string>("company_id", setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_company_settings, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            //take the single element without the array can be used by this below line
            //  var rootobject = JsonConvert.DeserializeObject<Person>(JsonResult);
            //take  the single element with the array can be used by this below line
            // this is to take the values from the array it trim the array holder from the json result
            string result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Login_user_Details>(result.ToString());
            string companyname = rootobject.company_name;
            var s = new FormattedString();
            s.Spans.Add(new Span { Text = companyname + " ", ForegroundColor = Color.FromHex("#141c77"), FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) });
            s.Spans.Add(new Span { Text = "Timesheet", ForegroundColor = Color.FromHex("#2d8e3d"), FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) });
            title.FormattedText = s;
            //to load the image from the database
            string logo = rootobject.logo;
            //company_logo
            image.Source = ImageSource.FromUri(new Uri(logo));
        }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
