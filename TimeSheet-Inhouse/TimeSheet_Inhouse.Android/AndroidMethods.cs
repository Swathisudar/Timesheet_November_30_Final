﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TimeSheet_Inhouse.Droid;
using Plugin.CurrentActivity;
using Android.Support.Design.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidMethods))]
namespace TimeSheet_Inhouse.Droid
{
    
   public class AndroidMethods : IAndroidMethods
    {

        private static Context context;

        public void CloseApp()
        {
            //Toast.MakeText(context, "press back to Exit ", ToastLength.Short).Show();
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
          
        }

        public void ShowSnackbar(string message)
        {
            Activity activity = CrossCurrentActivity.Current.Activity;
            Android.Views.View activityRootView = activity.FindViewById(Android.Resource.Id.Content);
            Snackbar.Make(activityRootView, message, Snackbar.LengthLong).Show();
        }
    }
}