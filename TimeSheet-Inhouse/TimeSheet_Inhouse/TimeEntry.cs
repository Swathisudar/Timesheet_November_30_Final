﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TimeSheet_Inhouse
{
   public class TimeEntry : ContentView
    {
        private object keyType;

        public Entry entry { get; set; }
        public Keyboard Keyboard { get; set; }

        public TimeEntry(string placeHolder)
        {
            var mainLayout = new RelativeLayout();
            this.Content = mainLayout;
            this.HeightRequest = 39;
            //this.BackgroundColor = Color.Green;

            
            entry = new Entry
            {

                Placeholder = placeHolder,

              
                //FontSize = Constants.H3
            };

            //  var underLine = new BoxView
            //  {
            //   BackgroundColor = Color.Gray,
            //   //HeightRequest = 1
            //  };


          
            mainLayout.Children.Add(entry,
                                   
                                     Constraint.Constant(0),
                                   
                                     Constraint.RelativeToParent((arg) => arg.Height));
            // mainLayout.Children.Add(underLine,
            //    Constraint.Constant(0),
            //   Constraint.RelativeToParent(p => p.Height - 2),
            //   Constraint.RelativeToParent((arg) => arg.Width),
            //   Constraint.Constant(1));
        }

        public static implicit operator TimeEntry(string v)
        {
            throw new NotImplementedException();
        }
    }
}
