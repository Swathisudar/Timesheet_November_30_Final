﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class OxyExData
    {
        private PlotModel modelP1;
        public PlotModel PieModel { get; set; }
        public string setcompid;
        public int sv_t, sb_t, ap_t, rj_t;
        public OxyExData()
        {

            if (App.Current.Properties.ContainsKey("save_t"))
                sv_t = (Int32)App.Current.Properties["save_t"];
            if (App.Current.Properties.ContainsKey("sub_t"))
                sb_t = (Int32)App.Current.Properties["sub_t"];
            if (App.Current.Properties.ContainsKey("appr_t"))
                ap_t = (Int32)App.Current.Properties["appr_t"];
            if (App.Current.Properties.ContainsKey("rej_t"))
                rj_t = (Int32)App.Current.Properties["rej_t"];




            PieModel = CreatePieChart();




        }







        private PlotModel CreatePieChart()
        {
            // var model = new PlotModel { Title = "Pie Chart" };


            modelP1 = new PlotModel { Title = "Pie Chart" };




            // int sav= (int)al.Get(0);









            var ps = new PieSeries
            {
                StrokeThickness = .25,
                InsideLabelPosition = .25,
                AngleSpan = 360,
                StartAngle = 0
            };
            // http://www.nationsonline.org/oneworld/world_population.htm
            // http://en.wikipedia.org/wiki/Continent
            ps.Slices.Add(new PieSlice("Saved", sv_t) { IsExploded = false });
            ps.Slices.Add(new PieSlice("Submitted", sb_t) { IsExploded = false });

            ps.Slices.Add(new PieSlice("Approved", ap_t) { IsExploded = false });
            ps.Slices.Add(new PieSlice("Rejected", rj_t) { IsExploded = false });
            modelP1.Series.Add(ps);
            return modelP1;


        }
    }
}
