﻿
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse
{
    public class Message
    {
        public Message(bool _isActive, int _intervalValue)
        {
            IsActivated = _isActive;
            time = _intervalValue;

        }
        public bool IsActivated { get; set; }
        public int time
        {
            get; set;
        }
       // public string company_name { get; set; }
    }
}
