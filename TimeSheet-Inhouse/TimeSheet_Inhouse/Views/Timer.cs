﻿using AdvancedTimer.Forms.Plugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace TimeSheet_Inhouse.Views
{
    public class Timer : ContentPage
    {

        Label label;
        Stopwatch mStopWatch = new Stopwatch();
        bool value,backset;
        string time_value;
        Image but_start, but_pause, but_stop;
        public Timer()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            label = new Label()
            {
                Text = "00:00:00",
                TextColor = Color.DarkBlue,
                FontSize = Device.GetNamedSize(NamedSize.Large,typeof(Label)),
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            //** start Image
            but_start = new Image
            {
                Source = "playnew6.png",

                HorizontalOptions = LayoutOptions.Center
            };

            //Creating TapGestureRecognizers  
            var tap_startImage = new TapGestureRecognizer();
            //Binding events  by the tap_startImage_Tapped;
            tap_startImage.Tapped += tap_startImage_Tapped;
            //Associating tap events to the image buttons  
            but_start.GestureRecognizers.Add(tap_startImage);

            //** pause
            but_pause = new Image
            {
                Source = "pause_2.png",

                HorizontalOptions = LayoutOptions.Center
            };
            but_pause.IsVisible = false;
            var tap_pauseImage = new TapGestureRecognizer();
            //Binding events  
            tap_pauseImage.Tapped += tapImage_pause_Tapped;
            //Associating tap events to the image buttons  
            but_pause.GestureRecognizers.Add(tap_pauseImage);
            //** stop Image
            but_stop = new Image
            {
                Source = "stop_3.png",
                HorizontalOptions = LayoutOptions.Center
            };
            but_stop.IsVisible = true;
            var tap_stopImage = new TapGestureRecognizer();
            //Binding events  
            tap_stopImage.Tapped += tapImage_stop_Tapped;
            //Associating tap events to the image buttons  
            but_stop.GestureRecognizers.Add(tap_stopImage);

            //** layout Design for the Page
            StackLayout stack = new StackLayout()
            {
                Padding = new Thickness(20, 50, 20, 0),
                Spacing = 20,
                Children =
                      {
                         new Frame
                         {
                              OutlineColor = Color.Silver,
                           HeightRequest=300,
                          

                         //Content = p_lab is possible 
                        //Content =p_lab,t_lab1 is not possible -> ie.)multiple things not goes into the Frame
                          // so that we go for the stacklayout => frame{stacklayout{p_label,t_label,....}}    
                             Content= new StackLayout
                             {

                                 Children=

              {
                                     new Frame
              {
                   OutlineColor=Color.Red,
                   Content=new StackLayout
                   {
                       Children=
                       {
                label
                       }

                   } },
                            new StackLayout
                   {
                                  Padding = new Thickness(0, 25, 0, 0),
                                 Orientation = StackOrientation.Horizontal,
                       Children=
                       {
                         but_start,but_pause,but_stop
                       }

                   }

                       },
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.White
                                 }
                             }
           }
            };
            Content = stack;
        }
        //----
        //method for start
     public   void tap_startImage_Tapped(object sender, EventArgs e)
        {
            backset = true;
            but_start.IsVisible = false;
            but_pause.IsVisible = true;
            but_stop.IsVisible = true;
            mStopWatch.Start();
            stopWatchControl(true);

        }

        //----
        //method for pause
        void tapImage_pause_Tapped(object sender, EventArgs e)
        {
            backset = true;
            mStopWatch.Stop();
            // but_start.IsVisible = true;
            but_start.Source = "replay1.png";
            but_start.IsVisible = true;
            but_pause.IsVisible = false;
            but_stop.IsVisible = true;
            stopWatchControl(false);
            //but_start.Text = "RESTART";
            var a = mStopWatch.Elapsed;
            label.Text = string.Format("{0:hh\\:mm\\:ss}", a);
        }
        //----

        //method for stop
        async void tapImage_stop_Tapped(object sender, EventArgs e)
        {
            backset = false;
            but_start.IsVisible = true;
            but_pause.IsVisible = true;
            but_stop.IsVisible = false;
            mStopWatch.Stop();
            stopWatchControl(false);

            //Value to be used
            var a = mStopWatch.Elapsed;
             time_value = string.Format("{0:hh\\:mm}", a);
            //Insert alert here

            mStopWatch.Reset();
            var a2 = mStopWatch.Elapsed;
            var a1 = string.Format("{0:hh\\:mm\\:ss}", a2);
            label.Text = string.Format("{0:hh\\:mm\\:ss}", a);
            //but_start.Text = "START";
            //alert came from the method
            sendtimervalue();

            
        }
        //------
        //** stop watch control method
        public void stopWatchControl(bool value)//1. true it return =>false  //2. false  elapsed and return true
        {
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (value) // if(false)
                {

                    var a = mStopWatch.Elapsed;
                    label.Text = string.Format("{0:hh\\:mm\\:ss}", a);
                    return true;
                }
                return false;
            });
        }
        public async void sendtimervalue()
        {
            bool ans = await DisplayAlert("Confirm?", "Would you like to submit the Working Hours: " + time_value.ToString(), "Ok", "Reset?");

            if (ans == true)
            {
                //redirect to different page
                MessagingCenter.Send<Timer, string>(this, "Hi", time_value.ToString());

                await Navigation.PopAsync();

            }
            else
            {
                mStopWatch.Reset();
                but_start.Source = "playnew6.png";
                but_start.IsVisible = true;
                but_pause.IsVisible = false;
                but_stop.IsVisible = true;

            }
            
        }
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if(backset==true)
                {
                    return;
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                       // Helpers.Settings.DisplayTimerstatus = false;
                        await Navigation.PopAsync();
                    }

                }

            });
           
            return true;
        }

        
    }
}
// var result = DisplayAlert("Alert!", "You want to exit the timer?", "yes", "no");
                    //if (await result)
                  //  {
                        //if (Device.OS == TargetPlatform.Android)
                      //  {
                          //  await Navigation.PopAsync();
                       // }
                 //   }
                   // else
                   // {
                       
                      //  return;
                  //  }