﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Plugin.Geolocator;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Timesheet_Leave : ContentPage
    {
        string a, b, content, setempid, JsonResult, id_reject, reject_desc, setcompid;
        public Timesheet_Leave()
        {

            InitializeComponent();
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            listview_leave.IsVisible = false;
            //getdate();


            LoadData();
            //listview_leave.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            //{
            //    Itemleave item = (Itemleave)e.SelectedItem;
            //    string val = item.id;
            //};

        }



        private void OnApprove(object sender, EventArgs e)
        {

            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as Itemleave;
                if (visit != null)
                {
                    string id = visit.id.ToString();
                    approve_leave(id);
                }
                else
                {
                   // DisplayAlert("ok", "gfghhj", "ok");
                }
            }
            else
            {
                return;
            }


        }




        private void ok_clicked(object sender, EventArgs e)
        {
            string Rejected_id = id_reject;
            reject_desc = EnteredName.Text;

            overlay.IsVisible = false;
            Reject_leave(Rejected_id);
        }

    
      

        private void onItemTapped(object sender, ItemTappedEventArgs e)
        {

            if (((ListView)sender).SelectedItem != null)
            {
                listview_leave.SelectedItem = null;
            }
            else
            {

            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        private void OnReject(object sender, EventArgs e)
        {
            overlay.IsVisible = true;
            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as Itemleave;
                if (visit != null)
                {
                    id_reject = visit.id.ToString();
                    // DisplayAlert("ok", visit.Dateend.ToString(), "ok");

                }
                else
                {
                    
                }
            }
            else
            {
                return;
            }
        }

        public async void LoadData()
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                string no = "No";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.get_holiday, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                try
                {

                    if (no.Equals(JsonResult))
                    {
                        no_message.IsVisible = true;

                        no_message.Text = "No Data are available ";
                        // no_message.TextColor = Color.Black;
                        listview_leave.IsVisible = false;
                    }
                    else
                    {
                        //string result = JsonResult.Trim(']').Trim('[');
                        //await DisplayAlert("okay ", result, "ok");
                        var rootobject = JsonConvert.DeserializeObject<List<Itemleave>>(JsonResult.ToString());
                        //await DisplayAlert("okay", rootobject.ToString(),"ok");
                        listview_leave.ItemsSource = rootobject;
                        listview_leave.IsVisible = true;
                        no_message.IsVisible = false;
                    }

                }
                catch (Exception e)
                {

                }




            }else
            {
                await DisplayAlert("Oh Snap!", "Employee Leave won't be loaded until you Connect to Internet", "Ok");
            }
        }

        private async void approve_leave(string id)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                try
                {
                    string user_id = Helpers.Settings.Displayemployeeid;
                    var client = new System.Net.Http.HttpClient();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


                    var postData = new List<KeyValuePair<string, string>>();
                    postData.Add(new KeyValuePair<string, string>("id", id));
                    postData.Add(new KeyValuePair<string, string>("approved_by", user_id));

                    var content = new System.Net.Http.FormUrlEncodedContent(postData);
                    var response = await client.PostAsync(Constants.approve_holiday, content);
                    //to take the value direstly without the array 
                    JsonResult = response.Content.ReadAsStringAsync().Result;
                    await DisplayAlert("Success!", JsonResult, "Ok");
                    LoadData();

                }
                catch (Exception e)
                {

                }
            }else
            {
                await DisplayAlert("Oh Snap!", "Approve the Leave after you Connect to Internet", "Ok");
            }
        }
        private async void Reject_leave(string id)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));

                try
                {
                    string user_id = Helpers.Settings.Displayemployeeid;
                  //  await DisplayAlert("Id", id, "okay");
                    var postData = new List<KeyValuePair<string, string>>();
                    postData.Add(new KeyValuePair<string, string>("id", id));
                    postData.Add(new KeyValuePair<string, string>("approved_by", user_id));

                    var content = new System.Net.Http.FormUrlEncodedContent(postData);
                    var response = await client.PostAsync(Constants.Reject_holiday, content);
                    //to take the value direstly without the array 
                    JsonResult = response.Content.ReadAsStringAsync().Result;
                    LoadData();
                    await DisplayAlert("Success!", JsonResult, "Ok");

                }
                catch (Exception e)
                {

                }
            }else
            {
                await DisplayAlert("Oh Snap!", "Reject the Leave after you Connect to Internet", "Ok");
            }
        }
        protected override bool OnBackButtonPressed()
        {
            var locator = CrossGeolocator.Current;
            //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


            bool getval = Helpers.Settings.DisplaySwitchstatus;
            if (locator.IsGeolocationEnabled && getval == true)
            {
                if (Device.OS == TargetPlatform.Android)
                {
                    DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                }
                Helpers.Settings.DisplayExitstatus = false;
               // return Helpers.Settings.DisplayExitstatus;
            }
            else
            {

                Helpers.Settings.DisplayExitstatus = true;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                    if (await result)
                    {
                        Helpers.Settings.DisplayLoginstatus = true;
                        await Navigation.PushAsync(new Report_injury());
                    }
                    else
                    {
                        string empname;
                        if (App.Current.Properties.ContainsKey("employeename"))
                        {
                            empname = (string)App.Current.Properties["employeename"];
                            Helpers.Settings.DisplayLoginstatus = true;
                            var a = Helpers.Settings.DisplayLoginstatus;
                            // await  DisplayAlert("Empname and status", empname+a, "ok");
                        }



                        if (Device.OS == TargetPlatform.Android)

                            DependencyService.Get<IAndroidMethods>().CloseApp();

                        //    // base.OnBackButtonPressed();
                        //
                    }

                });
                return Helpers.Settings.DisplayExitstatus;
            }


            return true;




        }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}