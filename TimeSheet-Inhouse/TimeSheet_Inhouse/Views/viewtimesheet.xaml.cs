﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeSheet_Inhouse.Helpers;
using System.Net.Http.Headers;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewTimesheet : ContentPage
    {
        public ListView listview;
        DateTime datevalu2;
        DateTime date1;
        DateTime date2;
        DateTime date3;
        DateTime date6;
        DateTime date4;
        DateTime date5;
        DateTime date7;
        string sunday;
        string monday;
        string tuesday;
        string wednesday;
        string thrusday;
        string friday;
        string saturday;
        class Person
        {
            public Person(string Day, string Date)
            {
                this.Day = Day;
                this.Date = Date;
               
            }
            public string Day { private set; get; }

            public string Date { private set; get; }

           
        };
        public ViewTimesheet()
        {
            InitializeComponent();
            this.Title = "View the Timesheet";
            Label header = new Label
            {
                Text = "View Timesheet for a week",
                FontSize = 20,
                TextColor=Color.Black,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Center
            };

            datevalu2 = DateTime.Today.Date;
            string value = datevalu2.DayOfWeek.ToString();
            //  string value1=datevalue.Date.Add(datevalue.Date)
            DisplayAlert("ok", datevalu2.ToString(), "ok");
            if (value == "Sunday")
            {
                date1 = datevalu2.AddDays(0);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");
                date2 = datevalu2.AddDays(1);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");
                date3 = datevalu2.AddDays(2);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(3);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(4);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(5);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(6);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }

            else if (value == "Monday")
            {
                date1 = datevalu2.AddDays(-1);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(0);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(1);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(2);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(3);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(4);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(5);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }

            else if (value == "Tuesday")
            {
                date1 = datevalu2.AddDays(-2);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(-1);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(0);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(1);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(2);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(3);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(4);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }

          else  if (value == "Wednesday")
            {
                date1 = datevalu2.AddDays(-3);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(-2);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(-1);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(0);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(1);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(2);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(3);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }
           else if (value == "Thursday")
            {
                date1 = datevalu2.AddDays(-4);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(-3);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(-2);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(-1);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(0);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(1);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(2);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }
          else  if (value == "Friday")
            {
                date1 = datevalu2.AddDays(-5);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(-4);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(-3);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(-2);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(-1);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(0);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(1);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }
           else if (value == "Saturday")
            {
                date1 = datevalu2.AddDays(-6);
                sunday = date1.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "sunday " + sunday, "ok");

                date2 = datevalu2.AddDays(-5);
                monday = date2.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "monday " + monday, "ok");

                date3 = datevalu2.AddDays(-4);
                tuesday = date3.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "tuesday " + tuesday, "ok");

                date4 = datevalu2.AddDays(-3);
                wednesday = date4.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "wednesday " + wednesday, "ok");

                date5 = datevalu2.AddDays(-2);
                thrusday = date5.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "thrusday " + thrusday, "ok");

                date6 = datevalu2.AddDays(-1);
                friday = date6.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "friday " + friday, "ok");

                date7 = datevalu2.AddDays(0);
                saturday = date7.ToString("yyyy'-'MM'-'dd");
                DisplayAlert("", "saturday " + saturday, "ok");
                senddate();
            }

            listview = new ListView {
               
                ItemTemplate = new DataTemplate(() =>
                {
                    Label projectname = new Label() {
                        Font = Font.SystemFontOfSize(NamedSize.Medium)
              .WithAttributes(FontAttributes.Bold),
                        TextColor=Color.Black,
                    };


                    projectname.SetBinding(Label.TextProperty, "Day");
                    //Label Taskname = new Label();
                    //Taskname.SetBinding(Label.TextProperty, "categories");
                    //Label projectdate = new Label();
                    //projectdate.SetBinding(Label.TextProperty, "latitude");
                    Label totalhours = new Label()
                    {
                        Font = Font.SystemFontOfSize(NamedSize.Medium)
              .WithAttributes(FontAttributes.Bold),
                    };
                    totalhours.SetBinding(Label.TextProperty, "Date");
                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                           
                            Orientation = StackOrientation.Horizontal,
                            Spacing = 2,
                            Children = {
                                new StackLayout {
                                     Padding= new Thickness(20,5,0,5),
                                    HorizontalOptions=LayoutOptions.StartAndExpand,
                                    Orientation=StackOrientation.Vertical,
                                    Spacing=2,
                                    Children= {
                                   new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,
                                        Children = {
                                    new Label { Text = "Project:", HorizontalOptions = LayoutOptions.FillAndExpand },
                                   projectname
                                }
                            },
                           
                                 
                                         
                                        
                                    }
                              
                                },
                            
                            new StackLayout
                             {
                                Padding=new Thickness(0,25,30,5),
                                 Orientation=StackOrientation.Vertical,
                                 HorizontalOptions=LayoutOptions.EndAndExpand,
                                 Spacing=2,
                                 Children={
                                        
                                        totalhours
                                 }

                         }

                        }
                        }
                        };
                

                })

            };
            listview.ItemSelected += OnSelection;
            listview.RowHeight = 70;
            listview.SeparatorColor = Color.LightBlue;
            this.Content = new StackLayout {
              
                Children =
                {
                   
                   header,
                    listview
                }
            };
        }

        private async void senddate()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("Sunday", sunday));
            postData.Add(new KeyValuePair<string, string>("Monday", monday));
            postData.Add(new KeyValuePair<string, string>("Tuesday", tuesday));
            postData.Add(new KeyValuePair<string, string>("Wednesday", wednesday));
            postData.Add(new KeyValuePair<string, string>("Thursday", thrusday));
            postData.Add(new KeyValuePair<string, string>("Friday", friday));
            postData.Add(new KeyValuePair<string, string>("Saturday", saturday));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/viewdate_timsheet", content);
            //to take the value direstly without the array 
            string JsonResult = response.Content.ReadAsStringAsync().Result;
            var rootobject = JsonConvert.DeserializeObject<List<Personget>>(JsonResult.ToString());
            listview.ItemsSource = rootobject;
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                BackgroundColor = Color.Red;
            }
            else
            {
                BackgroundColor = Color.BurlyWood;
            }
         
       
            //if (e.SelectedItem == null)
            //{
            //    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            //}
            //var value = e.SelectedItem.ToString();
            //DisplayAlert("Item Selected", value, "Ok");
        }
        public async void LoadData()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/laravel-test/public/product_get";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<Person>>(content);
            listview.ItemsSource = Items;
        }
    }

    internal class Personget
    {
        public string Day { get; set; }
        public string Date { get; set; }
    }
}
