﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using TimeSheet_Inhouse.Helpers;
using Xamarin.Forms;
using System.Net.Http.Headers;
using System.Net.Http;
using TimeSheet_Inhouse;

namespace TimeSheet_Inhouse
{
    public partial class App : Application
    {

        public int n = 20;
      
        public static object Navigation { get; internal set; }

        public App()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Connected"))
            {
                InitializeComponent();
                bool getval;
                string accessemployee;
                string emplname_pass, em_id_pass, com_id_pass, access1, email_pass, active_status_login, active_status;
              
                emplname_pass = Helpers.Settings.DisplayEmployeename;
                em_id_pass = Helpers.Settings.Displayemployeeid;
                com_id_pass = Helpers.Settings.Displaycompanyid;
                access1 = Helpers.Settings.Displaycontrolrole;
                email_pass = Helpers.Settings.Displayemail;
                getval = Helpers.Settings.DisplayLoginstatus;
                accessemployee = Helpers.Settings.DisplayAccesscontrolstatus;
                active_status_login = Helpers.Settings.DisplayActiveEmailLoginstatus;
                active_status = Helpers.Settings.DisplayActiveEmailstatus;
               
                if (networkStatus.Equals("Connected"))
                {

                    if (getval == true)
                    {



                        if (string.IsNullOrEmpty(emplname_pass))
                        {
                            MainPage = new NavigationPage(new HomePage());

                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("*********&&&&&&&&& -App.xaml- Settingclass help   &&&&&&&&&&&&&&&&&&&&&&&");


                            //App-Properties
                            //string empname = (string)App.Current.Properties["employeename"];
                            // string emp_id = (string)App.Current.Properties["employeeid"];
                            //string company_id = (string)App.Current.Properties["companyid"];
                            //string email_id = (string)App.Current.Properties["email"];
                            //string access_control = (string)App.Current.Properties["controlaccess"];
                            //// string acc2 = (string) App.Current.Properties["accessing"];
                            // System.Diagnostics.Debug.WriteLine(email_id);



                            System.Diagnostics.Debug.WriteLine(emplname_pass);
                            System.Diagnostics.Debug.WriteLine(em_id_pass);
                            System.Diagnostics.Debug.WriteLine(com_id_pass);

                            System.Diagnostics.Debug.WriteLine(email_pass);
                            System.Diagnostics.Debug.WriteLine(access1);
                            System.Diagnostics.Debug.WriteLine(accessemployee);
                            System.Diagnostics.Debug.WriteLine(active_status_login);
                            System.Diagnostics.Debug.WriteLine(active_status);

                            if (accessemployee != null)
                            {
                                if (accessemployee == access1 && active_status_login == active_status)
                                {
                                    Helpers.Settings.DisplayEmployeename = emplname_pass;
                                    MainPage = new NavigationPage(new Enter_timesheet(emplname_pass, com_id_pass, em_id_pass, email_pass, access1));

                                }

                                else
                                {

                                    MainPage = new NavigationPage(new HomePage());
                                }
                            }
                            else
                            {

                                MainPage = new NavigationPage(new HomePage());
                            }

                        }//condition finish

                    }


                    else
                    {
                        MainPage = new NavigationPage(new HomePage());
                    }
                }
                else
                {
                    MainPage = new NavigationPage(new HomePage());
                }
            }
            else
            {
                MainPage = new NavigationPage(new Views.help());
            }
        }
    


        protected override void OnStart()
        { 
        }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await App.Current.MainPage.DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
               
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }




        protected override void OnSleep()
        {
            SavePropertiesAsync();
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {

            // Handle when your app resumes
        }
        



    }
}