﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeSheet_Inhouse.Helpers;
using Plugin.Geolocator;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class help : ContentPage

    {
        ViewModel.ListContentViewModel listcontentVM;
        public help()
        {
            InitializeComponent();
            this.Title = "Help";

            //Settings.isLoggedIn = true;
            listcontentVM = new ViewModel.ListContentViewModel();

            listforlistcontent.ItemsSource = listcontentVM.listcontent1;
            listforlistcontent.ItemTapped += OnItemTapped;

        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (((ListView)sender).SelectedItem != null)
            {
                listforlistcontent.SelectedItem = null;
            }
            else
            {

            }
        }
        protected override bool OnBackButtonPressed()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


                bool getval = Helpers.Settings.DisplaySwitchstatus;
                if (locator.IsGeolocationEnabled && getval == true)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                    }
                    Helpers.Settings.DisplayExitstatus = false;
                    // return Helpers.Settings.DisplayExitstatus;
                }
                else
                {

                    Helpers.Settings.DisplayExitstatus = true;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                        if (await result)
                        {
                            Helpers.Settings.DisplayLoginstatus = true;
                            await Navigation.PushAsync(new Report_injury());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                        }
                        else
                        {
                            string empname;
                            if (App.Current.Properties.ContainsKey("employeename"))
                            {
                                empname = (string)App.Current.Properties["employeename"];
                                Helpers.Settings.DisplayLoginstatus = true;
                                var a = Helpers.Settings.DisplayLoginstatus;
                                // await  DisplayAlert("Empname and status", empname+a, "ok");
                            }

                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");

                            if (Device.OS == TargetPlatform.Android)

                                DependencyService.Get<IAndroidMethods>().CloseApp();

                            //    // base.OnBackButtonPressed();
                            //
                        }

                    });

                }
                base.OnBackButtonPressed();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("************************************************");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("************************************************");
            }
            System.Diagnostics.Debug.WriteLine("************************************************");
            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
            System.Diagnostics.Debug.WriteLine("************************************************");
            return true;
        }


    }
}
