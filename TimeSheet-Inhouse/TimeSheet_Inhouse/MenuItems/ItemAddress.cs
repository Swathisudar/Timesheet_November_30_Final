﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
   public class ItemAddress
    {
        public string area_name { get; set; }
        public ItemAddress(string area_name)
        {
            this.area_name = area_name;
        }
    }
}
