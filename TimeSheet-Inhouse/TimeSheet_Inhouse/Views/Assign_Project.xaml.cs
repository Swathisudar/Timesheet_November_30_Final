﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Assign_Project : ContentPage
    {
        public Picker employeepicker, rolepicker, projectpicker, taskpicker, taskstatuspicker;
        public DatePicker from_date_picker, to_date_picker;
        public Label select_emp, select_proj, select_task, select_role, select_status, assigntitle, assignedby, fromdate, todate, taskdescr;
        public Entry descr;
        public string employeeselected, selected_emp_id, descr_value, assign_value, setempid,setempname;
        public Assign_Project()
        {
            InitializeComponent();
            this.Title = "Assigning Project to Employee";
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            setempname= (string)App.Current.Properties["employeename"];
            DisplayAlert("ok", setempname, "ok");
            LoadUsers();
            LoadStatus();
            LoadProjectRoll();
            LoadProject();
            //--------------------Picker
            employeepicker = new Picker
            {
                Title = "select employee",
                WidthRequest = 250,
                VerticalOptions = LayoutOptions.FillAndExpand

            };
            // employeepicker.ItemDisplayBinding = new Binding("name");

            //--------------------------------------

            employeepicker.ItemDisplayBinding = new Binding("project_role");

            employeepicker.SelectedIndexChanged += (sender, args) =>
            {
                employeeselected = employeepicker.Items[employeepicker.SelectedIndex];
                //  DisplayAlert("employee string ", employeeselected, "ok");
                string[] values = employeeselected.Split('-');
                //  DisplayAlert("Selected Id", values[1], "ok");
                selected_emp_id = values[1];

            };


            rolepicker = new Picker
            {
                Title = "select role",
                WidthRequest = 250,
                VerticalOptions = LayoutOptions.StartAndExpand
            };
            rolepicker.ItemDisplayBinding = new Binding("project_role");

            projectpicker = new Picker
            {
                Title = "select project",
                WidthRequest = 250,

                VerticalOptions = LayoutOptions.StartAndExpand
            };

            //--------------------************** picker Depending Binding **************-------------------------
          //  projectpicker.ItemDisplayBinding = new Binding("project_name");//project name picker
            projectpicker.SelectedIndexChanged += (sender, args) =>
            {
                string projectselected = projectpicker.Items[projectpicker.SelectedIndex];

                LoadTask(projectselected);

            };



            //----------------------------------*******************************************-------------------------------

            taskpicker = new Picker
            {
                Title = "select task",
                WidthRequest = 250,

                VerticalOptions = LayoutOptions.StartAndExpand
            };

            taskpicker.ItemDisplayBinding = new Binding("task_name");




            taskstatuspicker = new Picker
            {
                Title = "select status",

                WidthRequest = 250,

                VerticalOptions = LayoutOptions.FillAndExpand
            };
            taskstatuspicker.ItemDisplayBinding = new Binding("task_status");

            //-------------------------button
            var submit = new Button
            {
                Text = "Submit",
                BackgroundColor = Color.FromHex("#1A52BA"),
                TextColor = Color.White,

                BorderRadius = 20,
                HorizontalOptions = LayoutOptions.Center

            };

            //------------------------------label
            select_emp = new Label
            {
                Text = "Employee : ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            select_role = new Label
            {
                Text = "Role for Project : ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.StartAndExpand
            };



            select_proj = new Label
            {
                Text = "Project : ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            select_task = new Label
            {
                Text = "Task:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            select_status = new Label
            {
                Text = "Task Status:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            taskdescr = new Label
            {
                Text = "Task Description:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            assigntitle = new Label
            {
                Text = "Assigned By: ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            assignedby = new Label
            {
                // Text = "name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            assign_value = (string)App.Current.Properties["employeename"];
            assignedby.Text = assign_value;
            fromdate = new Label
            {
                Text = "Start Date:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            todate = new Label
            {
                Text = "End Date:  ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            //--------------date picker

            from_date_picker = new DatePicker()
            {
                Format = "MM'-'dd'-'yyyy",
                //MaximumDate = DateTime.Now,
                //  MinimumDate= DateTime.Now.AddDays(-7),

            };
            to_date_picker = new DatePicker()
            {
                Format = "MM'-'dd'-'yyyy",
                //MaximumDate = DateTime.Now,
                //  MinimumDate= DateTime.Now.AddDays(-7),

            };



            //------------------entry
            descr = new Entry
            {
                Placeholder = "Please, Explain about your today task"

            };


            //-------------------layout starts
            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = new Thickness(10, 30, 0, 10),

                BackgroundColor = Color.FromHex("E0E0E0"),

                Children = {
                    new Frame
                    {
                          BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content= new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                     Padding = new Thickness(0, 10, 0, 0),
                   Children=
                    {






                            select_emp,employeepicker,

                        

                    //new BoxView {
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    BackgroundColor = Color.Gray,

                    //    HeightRequest = 2,
                    //    WidthRequest=4,
                    //    Margin=10,




                    //},


                   
                           select_role,rolepicker

                        ,
                     

 //                   new BoxView {
 //                       HorizontalOptions = LayoutOptions.FillAndExpand,
 //                       BackgroundColor = Color.Gray,
 //                       HeightRequest = 2,
 //                       WidthRequest=4,
 //                       Margin=10,
 //},
                   
                          select_proj,projectpicker,


                       //new BoxView {
                       //    HorizontalOptions = LayoutOptions.FillAndExpand,
                       //    BackgroundColor = Color.Gray,

                       //    HeightRequest = 2,
                       //    WidthRequest=4,
                       //    Margin=10,




                       //}, 
                       //        select_task,taskpicker,


                       //new BoxView {
                       //    HorizontalOptions = LayoutOptions.FillAndExpand,
                       //    BackgroundColor = Color.Gray,

                       //    HeightRequest = 2,
                       //    WidthRequest=4,
                       //    Margin=10,




                       //},   
                       select_task,taskpicker,
                       select_status,taskstatuspicker,


                              taskdescr, descr,
                   
                    //  new BoxView {
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    BackgroundColor = Color.Gray,

                    //    HeightRequest = 2,
                    //    WidthRequest=4,
                    //    Margin=10,




                    //},


                       
                  

                       


                    //new BoxView {
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    BackgroundColor = Color.Gray,

                    //    HeightRequest = 2,
                    //    WidthRequest=4,
                    //    Margin=10,




                    //},
                      new BoxView {
                       HorizontalOptions = LayoutOptions.FillAndExpand,
                        BackgroundColor = Color.Gray,

                       HeightRequest = 2,
                       WidthRequest=4,
                       Margin=5,




                    },



                    new Frame
                    {
                          BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content=
 new StackLayout
                       {
                             Orientation = StackOrientation.Vertical,
                             Padding=new Thickness(0,10,0,0),

                             Spacing=10,
                           Children=
                           {
                    new StackLayout
                       {
                             Orientation = StackOrientation.Horizontal,

                             Spacing=10,
                           Children=
                           {
                              assigntitle,assignedby
                           }
                       },

                       new StackLayout
                       {
                             Orientation = StackOrientation.Horizontal,
                             Spacing=10,
                           Children=
                           {
                               fromdate,from_date_picker
                           }
                       },

                          new StackLayout
                       {
                             Orientation = StackOrientation.Horizontal,
                             Spacing=10,
                           Children=
                           {
                             todate, to_date_picker,
        }
                       },



















                      }
                    }, }











                   }//over all stack layout



               }


                   },//--frame ended
               submit
//---------------------------layout  for picker and task
                }//children

            };

            //----------scrollview ----------     inside scroll add the layout
            var scroll = new ScrollView
            {
                Content = layout
            };
            this.Content = scroll;
            //---------------------submit button function------------
            submit.Clicked += async (object sender, EventArgs e) =>
            {
                if (employeepicker.SelectedIndex == -1 || employeepicker.SelectedIndex == 0)
                {
                    await DisplayAlert("Boo!", "Please select the Name with employee Id ", "ok");
                }
                else if (rolepicker.SelectedIndex == -1)
                {
                    await DisplayAlert("Boo!", "Please select the role of an employee to that project", "ok");
                }
                else if (projectpicker.SelectedIndex == -1 || projectpicker.SelectedIndex == 0)
                {
                    await DisplayAlert("Boo!", "Please select the project", "ok");
                }
                else if (taskpicker.SelectedIndex == -1)
                {
                    await DisplayAlert("Boo!", "Please select the task", "ok");
                }
                else if (taskstatuspicker.SelectedIndex == -1)
                {
                    await DisplayAlert("Boo!", "Please select the taskstatus", "ok");
                }


                else
                {
                    var client = new HttpClient();

                    //await DisplayAlert("Assign name", assign_value, "ok");
                    descr_value = descr.Text;
                    //  await DisplayAlert("Description:", descr_value, "ok");
                    string role_select = rolepicker.Items[rolepicker.SelectedIndex];
                    string task_select = taskpicker.Items[taskpicker.SelectedIndex];
                    string project_select = projectpicker.Items[projectpicker.SelectedIndex];
                    string status_select = taskstatuspicker.Items[taskstatuspicker.SelectedIndex];
                    string from_date_value = from_date_picker.Date.ToString("yyyy'-'MM'-'dd");
                    string to_date_value = to_date_picker.Date.ToString("yyyy'-'MM'-'dd");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var postData = new List<KeyValuePair<string, string>>();
                    postData.Add(new KeyValuePair<string, string>("emp_id", setempid));
                    postData.Add(new KeyValuePair<string, string>("project_name", project_select));
                    postData.Add(new KeyValuePair<string, string>("task_name", task_select));
                    postData.Add(new KeyValuePair<string, string>("role_project", role_select));
                    postData.Add(new KeyValuePair<string, string>("task_description", descr_value));
                    postData.Add(new KeyValuePair<string, string>("task_status", status_select));
                    postData.Add(new KeyValuePair<string, string>("assigned_by", setempname));
                    postData.Add(new KeyValuePair<string, string>("start_date", from_date_value));
                    postData.Add(new KeyValuePair<string, string>("end_date", to_date_value));
                    var content = new System.Net.Http.FormUrlEncodedContent(postData);
                    var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/assign_project", content);
                    string Jsonvalue = response.Content.ReadAsStringAsync().Result;
                    await DisplayAlert("Success!",  Jsonvalue, "ok"); descr.Text = null;
                    taskstatuspicker.ItemDisplayBinding = new Binding("task_status"); rolepicker.ItemDisplayBinding = new Binding("project_role");
                    employeepicker.SelectedIndex = 0; projectpicker.SelectedIndex = 0;
                    projectpicker.SelectedIndex = 0;

                    //    projectpicker.SelectedIndex = -1;

                    // projectpicker.SelectedIndex = 0;// set the 1st value of Picker from Json i.e. others
                    //I tried this things one by one nothing workout
                    // projectpicker.Items.Add("select");  //Breaking
                    //projectpicker.ItemDisplayBinding = new Binding("project_name");  //Breaking
                    // projectpicker.SelectedIndex = -1;// Breaking


                }
            };
        }
        private void refresh_method()
        {
            //  employeepicker.SelectedIndex = 0;// If the Picker.SelectIndex=0 -> dynamic json 1st value should be (Select the Project) 
            //  picker.SelectedIndex = -1;
            //  task.SelectedIndex = -1;
            descr.Text = null;
            employeepicker.SelectedIndex = -1;
            rolepicker.SelectedIndex = -1;
            taskstatuspicker.SelectedIndex = -1;
            //  employeepicker.SelectedIndex = -1;
            //  rolepicker.SelectedIndex = -1;

            //   rolepicker.SelectedIndex = -1;
        }








        //---------------------------method
        public async void LoadProject()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/Ts_sample/public/api/projects";
            // var RestURL = "http://vps4.techiva.com/projects/Timesheet/public/api/projects";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            // content = await response.Content.ReadAsStringAsync();
            //  var Items = JsonConvert.DeserializeObject<List<Day_Edit_LoadSpinner>>(content);
            //  picker.Items.Add("select");
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            //var Items = JsonConvert.DeserializeObject<List<Day_Edit_LoadSpinner>>(content);
            projectpicker.Items.Add("select");
            string[] tokens = result.Split(',');

            for (int i = 0; i < tokens.Length; i++)
            {
                string a = tokens[i];

                string b = a.Trim('"').Trim('"');
                projectpicker.Items.Add(b);
            }

            //   projectpicker.ItemsSource = Items;
        }
        //--------------------------------********* Load Task method for Depending Spinner****************-------------------

        public async void LoadTask(string projectselected)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("project_name", projectselected));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/task_name", content);
            //to take the value directly without the array 
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            var Items = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult);
            taskpicker.ItemsSource = Items;
        }
        //------------------------************************************************************---------------------------------------------------------
        //------------------------*****************      LoadStatus ************************---------------------------------------------------------


        public async void LoadStatus()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/Ts_sample/public/api/taskstatus";
            // var RestURL = "http://vps4.techiva.com/projects/Timesheet/public/api/taskstatus";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<LoadStatus>>(content);
            //  picker.Items.Add("select");

            taskstatuspicker.ItemsSource = Items;
        }
        //------------------------************************************************************---------------------------------------------------------
        //------------------------*****************      LoadProject roll ************************---------------------------------------------------------


        public async void LoadProjectRoll()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/Ts_sample/public/api/projectrole";
            // var RestURL = "http://vps4.techiva.com/projects/Timesheet/public/api/projectrole";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<LoadStatus>>(content);
            //  picker.Items.Add("select");

            rolepicker.ItemsSource = Items;




        }
        //------------------------************************************************************----------------------------------------------------
        //------------------------*****************    Load Users ************************---------------------------------------------------------


        public async void LoadUsers()
        {
            var content = "";
            HttpClient client = new HttpClient();
            // var RestURL = "http://vps4.techiva.com/projects/Ts_sample/public/api/projectrole";
            var RestURL = "http://vps4.techiva.com/projects/Ts_sample/public/api/users";
            // var RestURL = "http://vps4.techiva.com/projects/Timesheet/public/api/users";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            // content = await response.Content.ReadAsStringAsync();

            //------------------
            //string result = content.Trim(']').Trim('[');
            //var rootobject = JsonConvert.DeserializeObject<Userdetails>(result.ToString());
            //var emp_name = rootobject.name;
            //var emp_id = rootobject.emp_id;

            //----------------------------
            string Jsonvalue = response.Content.ReadAsStringAsync().Result;
            string result = Jsonvalue.Trim(']').Trim('[');
            employeepicker.Items.Add("select Name - select ID");
            //  await DisplayAlert("jsonResult", result, "ok");
            string[] tokens = result.Split(',');

            for (int i = 0; i < tokens.Length; i++)
            {
                string a = tokens[i];
                string b = a.Trim('"').Trim('"');
                //   await  DisplayAlert("value ", b, "ok");
                // employeepicker.Items.Add("select Name -select ID");
                employeepicker.Items.Add(b);

                //string[] values = b.Split('-');
                //for(int j = 0; j < values.Length; i++)
                //{

                //}


            }

            //  var Items = JsonConvert.DeserializeObject<List<LoadStatus>>(content);


            //   employeepicker.ItemsSource = Items;

        }
        //------------------------************************************************************----------------------------------------------------



        protected override bool OnBackButtonPressed()
        {
            return true;
        }

    }
}