﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class LoadStatus
    {
        public string task_status { private set; get; }
        public string project_role { private set; get; }
        // public string user_name { private set;  get}

        public LoadStatus(string project_role, string task_status)
        {
            this.project_role = project_role;
            this.task_status = task_status;
        }
    }
}
