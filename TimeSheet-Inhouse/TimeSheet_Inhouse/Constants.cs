﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse
{
  public static  class Constants
    {
        public static string setcompid, setempid;
     
        //Home Page
        public static string Login_url = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/login";
       
        public static string get_company_details = "http://vps4.techiva.com/projects/Timesheet/public/api/company_details";
        public static string get_company_settings = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/company_settings";



        //Enter timesheet
        public static string get_role_url = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/roles";
        public static string get_active_status = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/active_status";





        //Day timesheet , weekly timesheet 
        public static string save_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/enter_timesheet_save";
        public static string submit_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/enter_timesheet_submit";
        public static string InjuryReport_Upload_url = "http://vps4.techiva.com/projects/laravel-test/public/imageupload";
        public static string task = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/task_by_project";
        public static string project = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/load_project";
        public static string Current_date = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/getting_date";
        public static string getting_Week_dates = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/getting_date";

        //current date
        public static string current_date = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/date";
        //View timesheet
        public static string submit_saved_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/update_pending_timesheet";
        public static string Current_week_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/thisweek";
        public static string Filter_thisweek_status = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/view-timesheet-thisweek-filter";
        public static string delete_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/delete_timesheet";
        public static string Filter_previousweek_status = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/view-timesheet-lastweek-filter";
        public static string previousweek_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/lastweek";
        public static string Filter_lastbeforeweek_status = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/view-timesheet-lastweek-before-filter";
        public static string lastbeforeweek_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/before_lastweek";
        //Approve- holiday
        public static string get_holiday = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/get_holiday";
        public static string approve_holiday = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/approve_holiday";
        public static string Reject_holiday = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/reject_holiday";
        //Approve - Timesheet
        public static string get_employee ="http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/get_employee";
        public static string get_pending_timesheet ="http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/get_timesheet";
        public static string Approve_timesheet ="http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/approve_timesheet";
        public static string Reject_timesheet ="http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/reject_timesheet";
        //Check-in_out
        public static string Delete_address = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/delete_address";
        public static string Check_in = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/check_in";
        public static string get_event_id = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/get_checkedin_user";
        public static string Check_out = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/check_out";
        public static string Edit_address = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/update_address";
        public static string Add_address = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/addaddress";
        public static string get_latlong_address = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/get_area_name";
        public static string load_user_added_address = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/getaddress";
        //Location update
        public static string location_update = "http://vps4.techiva.com/projects/Ts_sample/public/api/getting_location";
        // Edit timesheet:
        public static string Edittimesheet_submit ="http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/update_timesheet";
        // Get Total Timesheet for pie-chart
        public static string get_total_timesheet = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/chat_details";
        public static string injuryreport_submit = "http://vps4.techiva.com/projects/Techiva-Timesheet/public/api/injury_report ";
        //Database key values
        public static string company_id = "company_id";
        public static string emp_id = "emp_id";
        public static string project_name = "project_name";
        public static string task_name = "task_name";
        public static string hours = "hours";
        public static string task_description = "task_description";
        public static string date = "date";
        public static string approve_status = "approve_status";
        public static string id = "id";
        public static string dateconvert = "yyyy'-'MM'-'dd";
        public static string JsonFormat ="application/json";
        public static string address = "address";
        public static string parent_id = "parent_id";
        public static string latitude = "latitude";
        public static string longitude = "longitude";
        public static string notes = "notes";
        public static string user_id = "user_id";
        public static string approvestatus_submitted ="Submitted";
        public static string approvestatus_approved = "Approved";
        public static string approvestatus_saved = "Saved";
        public static string description = "description";
        public static string approvestatus_reject = "Rejected";       
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";
        //public static string company_id = "company_id";




    }
}
