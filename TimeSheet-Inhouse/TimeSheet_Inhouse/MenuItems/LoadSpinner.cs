﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class LoadSpinner
    {
        public LoadSpinner(string project_name, string task_name)
        {
            this.task_name = task_name;
            this.project_name = project_name;


        }
        public string task_name { private set; get; }
        public string project_name { private set; get; }
    }
}
