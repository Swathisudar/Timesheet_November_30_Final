﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class Itemleave
    {
        string dateformat;
        public Itemleave()
        {
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
        }
        public Itemleave(string id, string emp_id, string type, string start_date, string end_date)
        {
            this.id = id;
            this.emp_id = emp_id;
            this.start_date = start_date;
            this.end_date = end_date;
            this.type = type;
        }
        public string emp_id {  get; set; }
        public string type {  get; set; }
        // public string start_date { get; set; }
        //public string image { get; set; }
       
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string id {  get; set; }
        
        
        //public string Dateend
        //{
        //    get
        //    {
        //        return _fromDate.ToString("mm/dd/yyyy");

        //    }
        //    set
        //    {

        //    }
        //}
    }
}
