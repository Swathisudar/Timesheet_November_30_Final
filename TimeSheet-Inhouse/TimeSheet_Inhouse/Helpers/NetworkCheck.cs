﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.Helpers
{
    class NetworkCheck
    {
       
            public static async Task<bool> IsConnected()
        {
            bool status = CrossConnectivity.Current.IsConnected &&
                   await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (status == true)
            {
                return true;
            }
            return false;
        }
    }
}
