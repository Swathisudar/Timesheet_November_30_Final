﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Net;
using Plugin.Permissions;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.LocalNotifications;
using Plugin.LocalNotifications.Abstractions;
using Android.Content;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Android.Locations;
using TimeSheet_Inhouse;
using Xamarin.Forms.PlatformConfiguration;
using Plugin.Connectivity;

namespace TimeSheet_Inhouse.Droid
{
    [Activity(Label = "TimeSheet_Inhouse", Icon = "@drawable/applogotimesheet", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public int id;
        public static int n;


        #region Fields

        protected static ServiceConnection _serviceConnection;
        //   public const int Location_BroadCastTime = 20;//900 ; //20//Seconds; If you wants Minutes then, lets say 1 hours: 60 x 60 = 3600; 

        public int Location_BroadCastTime = n;//900 ; //20//Seconds; If you wants Minutes then, lets say 1 hours: 60 x 60 = 3600; 

        #endregion

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);


            #region Service Connections

            _serviceConnection = _serviceConnection ?? new ServiceConnection(null);
            _serviceConnection.ServiceConnected += _serviceConnection_ServiceConnected;
            _serviceConnection.ServiceDisconnected += _serviceConnection_ServiceDisconnected;


            #endregion

            global::Xamarin.Forms.Forms.Init(this, bundle);

           
            //string text = Intent.GetStringExtra("MyData") ?? "Data not available";
            ///Toast.MakeText(this, "u r in risk", ToastLength.Short).Show();
            OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();
            var isConnected = CrossConnectivity.Current.IsConnected;
            check_connection();
           
            LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
            {
                checkloc();
            }
            else
            {

            }
           
            MessagingCenter.Subscribe<TimeSheet_Inhouse.Message>(this, string.Empty, OnLocationActivate);
           
        }

        void OnLocationActivate(TimeSheet_Inhouse.Message response)
        {
            
            
                if (response.IsActivated)
                {
                LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

                if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
                {
                    checkloc();
                }
                else
                {
                    StartLocationService();
                }
                }
                else
                {
                //OnDestroy();
                   StopLocationService();
                }

                n = response.time;
           
              
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            // StopLocationService();

            //_serviceConnection.ServiceConnected -= _serviceConnection_ServiceConnected;
            //_serviceConnection.ServiceDisconnected -= _serviceConnection_ServiceDisconnected;
        }
        public void checkloc()
        {

           // LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

           // if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
           // {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Location");
                alert.SetMessage("GPS is disabled in your device. Would you like to enable it?");
                alert.SetPositiveButton("GPS settings", (senderAlert, args) =>
                {
                    Intent gpsSettingIntent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                    Forms.Context.StartActivity(gpsSettingIntent);
                    System.Threading.Thread.Sleep(200);
                    this.FinishActivity(3000);                   
                });

                alert.SetNegativeButton("Cancel", (senderAlert, args) =>
                {

                });
                Dialog dialog = alert.Create();
                dialog.Show();

           // }
        }

        #region Location Service Stuff

        private void _serviceConnection_ServiceDisconnected(object sender, ServiceConnectedEventArgs e)
        {
            if (Location != null)
            {
                Location.LocationChanged -= Location_LocationChanged;
            }


            Console.WriteLine(nameof(_serviceConnection_ServiceConnected) + " MainActivity ServiceConnection Service Disconnected");
        }

        private void _serviceConnection_ServiceConnected(object sender, ServiceConnectedEventArgs e)
        {
            Console.WriteLine(nameof(_serviceConnection_ServiceConnected) + " MainActivity ServiceConnection Service Connected");
            //_lblServiceStatus.Text = "Connected";
            //_lblServiceConnectedTime.Text = string.Format("Connected @ {0}", DateTime.Now.ToString(@"hh\:mm tt"));

            string bTime = "Broadcast Time: {0} {1}";
            if (Location_BroadCastTime > 59) //In case dynamic values 
            {
                bTime = string.Format(bTime, Location_BroadCastTime, "Min");
            }
            else
                bTime = string.Format(bTime, Location_BroadCastTime, "Sec");

            // _lblBroadcastTimeInterval.Text = bTime;

            //Service is connected and ready use.
            if (Location != null)
            {
                Location.LocationChanged += Location_LocationChanged;
            }
        }

        private void Location_LocationChanged(object sender, PositionEventArgs e)
        {
            //Console.WriteLine(nameof(Location_LocationChanged) + " Location " + e.Position.Latitude + " " + e.Position.Longitude);
            MessagingCenter.Send(e.Position, string.Empty);

        }

        public void StartLocationService()
        {
            new Task(() =>
            {
                StartService(new Intent(this, typeof(LocationService)));

                // bind our service (Android goes and finds the running service by type, and puts a reference
                // on the binder to that service)
                // The Intent tells the OS where to find our Service (the Context) and the Type of Service
                // we're looking for (LocationService)
                Intent locServiceIntent = new Intent(this, typeof(LocationService));

                // Finally, we can bind to the Service using our Intent and the ServiceConnection we
                // created in a previous step.
                BindService(locServiceIntent, _serviceConnection, Bind.AutoCreate);

                Console.WriteLine(nameof(StartLocationService) + " Started Location Service");

            }).Start();
            Notification.Builder builder = new Notification.Builder(this)
                                            .SetContentTitle("Location")
                                            .SetContentText("Techiva Timesheet starts tracking your location")
                                            .SetSmallIcon(Resource.Drawable.applogotimesheet);

            Notification notification = builder.Build();

            NotificationManager notificationManager = GetSystemService(NotificationService) as NotificationManager;

            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);

            

        }
        public void StopLocationService()
        {
            // Unbind from the LocationService; otherwise, StopSelf (below) will not work:
            if (_serviceConnection != null)
            {
                UnbindService(_serviceConnection);
            }
            // Stop the LocationService:
            if (Location != null)
            {
                Location.StopSelf();
            }
            Console.WriteLine(nameof(StopLocationService) + " Stopped Location Service");
            Notification.Builder builder = new Notification.Builder(this)
                                           .SetContentTitle("Location")
                                           .SetContentText("Techiva Timesheet stops tracking your location")
                                           .SetSmallIcon(Resource.Drawable.applogotimesheet);

            Notification notification = builder.Build();

            NotificationManager notificationManager = GetSystemService(NotificationService) as NotificationManager;

            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }
        #endregion


        #region Location Stuff

        public LocationService Location
        {
            get
            {
                if (_serviceConnection.Binder == null)
                    throw new Exception("Service not bound yet");

                //Note that we using the ServiceConnection to get the Binder, and the Binder to get the Service here
                return _serviceConnection.Binder.Service;
            }
        }

        #endregion
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        private void check_connection()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;

            if (isConnected)
            {
                LoadApplication(new App());
                //  Toast.MakeText(this, "You are currently connected to internet", ToastLength.Long).Show();
            }
            else
            {
                // Toast.MakeText(this, "You are currently NOT connected to internet", ToastLength.Long).Show();
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Internet!");
                alert.SetMessage("No Internet!. Please check your connection");
                alert.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    check_connection();
                });
                Dialog dialog = alert.Create();
                dialog.Show();
                return;

            }

        }
    }
    }


