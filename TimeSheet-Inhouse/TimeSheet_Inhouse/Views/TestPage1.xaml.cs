﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class TestPage1 : ContentPage, IElementConfiguration<Frame>
    {
        public Label header,STimer;
        public Picker picker,task;
        class Person
        {
            public Person(string task_name)
            {
                this.task_name = task_name;

            }
            public string task_name { private set; get; }

        };
        public TestPage1()
        {
            //to set the Title in the navigationbar
           
      
            this.Title = "Home";
            LoadData();
            Label heading = new Label
            {
                Text = "Enter Timesheet for a day",
                FontSize = 20,
                TextColor = Color.Black,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Center
            };
            header = new Label
            {
                Text = "Project Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Label header2 = new Label
            {
                Text = "Task Name",
                TextColor=Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Label header3 = new Label
            {
                Text = "Duration",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Label curdatetxt = new Label
            {
                Text = "Date",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var showdate = new Label {
                TextColor = Color.Black,
                Text = "Current Date:" + DateTime.Today.ToString("dd'/'MM'/'yyyy"),
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions =LayoutOptions.Center
            };
            var stime = new TimePicker()
            {

                Time = new TimeSpan(0, 0, 0),

                Format = "HH:mm"
            };

            //var stime = new Picker() {

            //};

             //LoadData();
          
             picker = new Picker
            {
                Title = "choose project",
                
                 VerticalOptions = LayoutOptions.CenterAndExpand
            };
           // picker.SelectedIndexChanged += this.myPickerSelectedIndexChanged;
            //To display the picker value from JSon
            // picker.ItemDisplayBinding = new Binding("productname");
            //To set the picker value default
            picker.Items.Add("electronics");
            picker.Items.Add("Bike");
            picker.Items.Add("fashion");
            picker.Items.Add("games");
            picker.Items.Add("others");
            picker.Items.Add("home_garden");

            task = new Picker
            {
                Title = "choose task",
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            task.ItemDisplayBinding = new Binding("task_name");
            
            var summit = new Button
            {
                Text = "Submit for Approval",
                BackgroundColor = Color.FromHex("#1A52BA"),
                TextColor = Color.White,
                BorderRadius = 2,
                HorizontalOptions=LayoutOptions.Center

            };
        STimer = new Label
           {
                Text ="Start Timer",
                TextColor=Color.Black,
           };
        // Set the OnclickListener for Label
            var STimer_tap = new TapGestureRecognizer();
            STimer_tap.Tapped += (s, e) => {
                Navigation.PushAsync(new TestPage2());
            };
            STimer.GestureRecognizers.Add(STimer_tap);
       // Set the UnderLine effects from Custom renderers
            STimer.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
        
            summit.Clicked += (object sender, EventArgs e) => {
                if (picker.SelectedIndex == -1)
                {
                    DisplayAlert("Alert!", "Please enter the project", "ok");
                }
                else if (task.SelectedIndex == -1)
                {
                    DisplayAlert("Alert!", "Please enter the task", "ok");
                }
                else {
                    //store the selected value from picker into string
                    //string value = picker.Items[picker.SelectedIndex];
                  // DisplayAlert("Alert!", value, "ok");
                    DisplayAlert("Alert!", "Sucessfully saved into Database", "Ok");
                    //send the value to the help page
                    Navigation.PushAsync(new TabbedPage1());
                }
            };



            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Children = {
               new StackLayout
            {

                Padding = new Thickness(0,10,0,0),
               Spacing=8,
                Children = {heading,
                new StackLayout {
                     Padding=new Thickness(20,3,20,0),
                   
                    Children= {
                        //Setting the Frame
                new Frame {
                  BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                   Spacing=1,
                Children=
                    {
                         header,picker,header2,task
                    }
                }

                }
                    }
                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),
                    
                    Children= {
                   new Frame
                {
                 OutlineColor=Color.White,
                    HasShadow=true,
                  BackgroundColor=Color.FromHex("FFFFFF"),
                     Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                Children=
                    {
                             new StackLayout {
                                 Orientation=StackOrientation.Horizontal,
                                
                                 Children= {
                            new StackLayout
                             {
                               // Padding=new Thickness(0,-3,30,0),
                                HorizontalOptions=LayoutOptions.EndAndExpand,
                                 Children={
                                    showdate
                                 }
                                       }
                                 }
                             },
                                    header3,stime
                    }
                                  }
                }
                       }
                    
                },
                new StackLayout {
                    Padding=new Thickness(260,2,0,0),
                    Children= {
                       STimer

                    }
                },
                       summit
                   }
            }
        }
            };
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
                IPlatformElementConfiguration<T, Frame> IElementConfiguration<Frame>.On<T>()
        {
            throw new NotImplementedException();
        }
        private void myPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = picker.Items[picker.SelectedIndex];
            postData(selectedValue);

        }
        public async void LoadData()
        {
            var content = "";
            HttpClient client = new HttpClient();
            // var RestURL = "http://vps4.techiva.com/projects/laravel-test/public/product_get";
            var RestURL = "http://vps4.techiva.com/projects/Timesheet/public/api/tasks";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<Person>>(content);
            task.ItemsSource = Items;
        }
        public async void postData(string pick) {
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string pick_select = picker.Items[picker.SelectedIndex];
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("categories",pick));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Letsgo_php/filter_categories.php", content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            var rootobject = JsonConvert.DeserializeObject<List<Person>>(JsonResult);
            task.ItemsSource = rootobject;
        }
        //public async void LoadData() {
        //    var client = new System.Net.Http.HttpClient();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    string pick_select= picker.Items[picker.SelectedIndex];
        //    DisplayAlert("dfgdg", pick_select, "dfdg");
        //    var postData = new List<KeyValuePair<string, string>>();
        //    postData.Add(new KeyValuePair<string, string>("categories", pick_select));
        //    var content = new System.Net.Http.FormUrlEncodedContent(postData);

        //    var response = await client.PostAsync("http://vps4.techiva.com/projects/Letsgo_php/filter_categories.php", content);

        //    var JsonResult = response.Content.ReadAsStringAsync().Result;
        //    var rootobject = JsonConvert.DeserializeObject<List<Person>>(JsonResult);
        //   // task.ItemsSource = rootobject;
        //}
        public static implicit operator View(TestPage1 v)
        {
            throw new NotImplementedException();
        }
    }
}
