﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Android.App.Usage;
using Plugin.Connectivity;
namespace TimeSheet_Inhouse.Droid
{
    [Activity(Theme = "@style/Theme.Splash",
          MainLauncher = true,
          NoHistory = true)]
    public class Spashscreen : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            
            base.OnCreate(savedInstanceState);
            System.Threading.Thread.Sleep(4000);
            check_connection();
            //ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
           
            //NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            //bool isOnline = networkInfo.IsConnected;

            //if (isOnline == true)
            //{
            //    Toast.MakeText(this, "internet! ", ToastLength.Short).Show();
            //    StartActivity(typeof(MainActivity));
            //}
            //else
            //{
            //    return;
            //}

            // Create your application here
        }
        public void checkloc()
        {
          
           
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Internet!");
                alert.SetMessage("No internet.Please Check your connection");
                alert.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
                    NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
                    bool isOnline = networkInfo.IsConnected;

                    if (isOnline == true)
                    {
                        //Toast.MakeText(this, "internet! ", ToastLength.Short).Show();
                        StartActivity(typeof(MainActivity));
                    }
                    else
                    {
                        checkloc();
                    }
                });

                //alert.SetNegativeButton("Cancel", (senderAlert, args) =>
                //{
                //    Toast.MakeText(this, "u r in risk", ToastLength.Short).Show();
                //});
                Dialog dialog = alert.Create();
                dialog.Show();

           
        }
        private void check_connection()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;

            if (isConnected)
            {
                StartActivity(typeof(MainActivity));
              //  Toast.MakeText(this, "You are currently connected to internet", ToastLength.Long).Show();
            }
            else
            {
               // Toast.MakeText(this, "You are currently NOT connected to internet", ToastLength.Long).Show();
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Internet!");
                alert.SetMessage("No Internet!. Please check your connection");
                alert.SetPositiveButton("OK", (senderAlert, args) =>
                {


                    check_connection(); 
                });
                Dialog dialog = alert.Create();
                dialog.Show();
                return;
                
            }

        }
    }
}