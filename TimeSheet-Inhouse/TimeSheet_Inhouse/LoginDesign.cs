﻿using Android.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TimeSheet_Inhouse
{
    public class LoginDesign : ContentView
    {
      public  Entry entry { get; set; }
        public LoginDesign(ImageSource imageSource, string placeHolder)
        {
            var mainLayout = new RelativeLayout();
            this.Content = mainLayout;
            this.HeightRequest = 39;
            //this.BackgroundColor = Color.Green;

            var image = new Image
            {
                Source = imageSource,
                WidthRequest = 30,
                HeightRequest = 30,
                //BackgroundColor = Color.Red
            };
          entry = new Entry
            {

                Placeholder = placeHolder,
                Keyboard=Keyboard.Email,
                //keyboardType = keyType,
                //FontSize = Constants.H3
            };
           
            //  var underLine = new BoxView
            //  {
            //   BackgroundColor = Color.Gray,
            //   //HeightRequest = 1
            //  };


            mainLayout.Children.Add(image,
                                     Constraint.Constant(0),
                                     Constraint.RelativeToParent((arg) => arg.Height / 2 - image.HeightRequest / 2));
            mainLayout.Children.Add(entry,
                                     Constraint.RelativeToView(image, (arg1, arg2) => arg2.Width + 13),
                                     Constraint.Constant(0),
                                     Constraint.RelativeToView(image, (arg1, arg2) => arg1.Width - arg2.Width - 13),
                                     Constraint.RelativeToParent((arg) => arg.Height));
            // mainLayout.Children.Add(underLine,
            //    Constraint.Constant(0),
            //   Constraint.RelativeToParent(p => p.Height - 2),
            //   Constraint.RelativeToParent((arg) => arg.Width),
            //   Constraint.Constant(1));
        }

    }
}
