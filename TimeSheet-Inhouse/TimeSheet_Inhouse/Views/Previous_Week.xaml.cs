﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Previous_Week : ContentPage
    {

        string emp_id, projectname, taskname, id, date, hours, status;
        string get_status, hrs_value, min_value, dateformat, setcompid;

        private async void Submit_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                var menuitem = sender as MenuItem;
                if (menuitem != null)
                {

                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                    if (visit != null)
                    {
                        status = visit.approve_status.ToString();
                        if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                        {

                            await DisplayAlert("Oh Snap!", "Your are not able to Submit, already your Timesheet is " + status, "Ok");
                            menuitem.IsDestructive = true;
                        }
                        if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                        {
                            var client = new HttpClient();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                            string id = visit.id.ToString();
                            var postData = new List<KeyValuePair<string, string>>();
                            postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.submit_saved_timesheet, content);
                            var JsonResult = response.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "Ok");
                            filter_show1.IsVisible = false;
                            listview12.IsVisible = false;

                            LoadData();
                        }
                    }
                }
            }
            else {
                await DisplayAlert("Oh Snap!", "Submit the Timesheet after you Connect to Internet", "Ok");
            }

        }

        private void onItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (((ListView)sender).SelectedItem != null)
            {
                listview12.SelectedItem = null;
            }
            else
            {

            }
        }

        private async void ImageClicked(object sender, EventArgs e)
        {
            //overlay.IsVisible = true;
            string statusselected;
            var action = await DisplayActionSheet("Filter the Timesheet?", "Cancel", null, "Submitted", "Saved", "Approved", "Rejected", "All");
            switch (action)
            {
                case "Submitted":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Saved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Approved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Rejected":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "All":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;

            }

        }

        //private void label_date21_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    var datelabel = sender as Label;

        //    if(datelabel!=null && (e.PropertyName == "Text"))
        //    {



        //    }
        //}

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        public Previous_Week()
        {
            InitializeComponent();
            this.Title = "Previous Week";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                emp_id = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(emp_id))
            {
                emp_id = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

          //  DisplayAlert("Prevois week companyid", setcompid, "okay");
            MessagingCenter.Subscribe<Edit_Timesheet>(this, "MyItemsChanged", sender => {
                LoadData();
            });
            filter_show1.IsVisible = false;
            LoadData();

            status2.IsVisible = false;
            status2.SelectedIndexChanged += (sender, args) =>
            {

                string statusselected = status2.Items[status2.SelectedIndex];


                filter_LoadData(statusselected);

            };
            listview12.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                if (e.SelectedItem != null)
                {
                    var value = e.SelectedItem.ToString();

                }
            };
            {

            };


        }


        //public void OnitemClicked_P(object sender,SelectedItemChangedEventArgs e)
        //{

        //    var obj = (sender as ListView).SelectedItem as ItemClass;
        //    obj.itemcolor = Color.Blue;
        //}
        private async void filter_LoadData(string statusselected)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                if (statusselected == "All")
                {
                    filter_show1.IsVisible = true;
                    filter_show1.Text = "All";
                    LoadData();
                }
                else
                {
                    string no = "No";
                    filter_show1.IsVisible = true;
                    filter_show1.Text = statusselected;
                    string status_sel = statusselected;
                    var client = new HttpClient();

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                    var postData = new List<KeyValuePair<string, string>>();

                    postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                    postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                    postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                    var content = new FormUrlEncodedContent(postData);
                    var response = await client.PostAsync(Constants.Filter_previousweek_status, content);
                    var JsonResult = response.Content.ReadAsStringAsync().Result;
                    if (no.Equals(JsonResult))
                    {
                        no_message.IsVisible = true;

                        no_message.Text = "No Timesheet are available";
                        // no_message.TextColor = Color.Black;
                        listview12.IsVisible = false;
                    }
                    else
                    {
                        string result = JsonResult.Trim(']').Trim('[');
                        var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                        listview12.IsVisible = true;
                        listview12.ItemsSource = rootobject;
                        overlay.IsVisible = false;
                        no_message.IsVisible = false;
                    }
                }
            }
            else
            {
                await DisplayAlert("Oh Snap!", "View the Filtered Timesheet after you Connect to Internet", "Ok");
            }
        }
        public async void LoadData()
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                string no = "No";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.previousweek_timesheet, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                //string result = JsonResult.Trim(']').Trim('[');
                if (no.Equals(JsonResult))
                {
                    no_message.IsVisible = true;

                    no_message.Text = "No Timesheet are available";
                    // no_message.TextColor = Color.Black;
                    listview12.IsVisible = false;
                }
                else
                {
                    var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                    listview12.ItemsSource = rootobject;
                    no_message.IsVisible = false;
                    listview12.IsVisible = true;
                }

            }
            else
            {
                await DisplayAlert("Oh Snap!", "View the Timesheet after you Connect to Internet", "Ok");
            }
        }
        private async void Edit_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                var menuitem = sender as MenuItem;
                if (menuitem != null)
                {

                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                    if (visit != null)
                    {
                        projectname = visit.project_name.ToString();
                        taskname = visit.task_name.ToString();
                        id = visit.id.ToString();
                        date = visit.date.ToString();
                        hours = visit.Hourstring.ToString();
                        string[] hrs = hours.Split(':');

                        for (int i = 0; i < hrs.Length; i++)
                        {
                            hrs_value = hrs[0];
                            min_value = hrs[1];

                        }
                        status = visit.approve_status.ToString();
                        if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                        {
                           await DisplayAlert("Oh Snap!", "Your are not able to Edit, already your Timesheet is " + status, "Ok");
                            menuitem.IsDestructive = true;
                        }
                        if (status.Equals("Rejected") || status.Equals("Saved"))
                        {
                           await Navigation.PushAsync(new Edit_Timesheet(projectname, taskname, id, hrs_value, min_value, date));
                        }
                        if (status.Equals("Approved"))
                        {
                            return;
                        }

                    }




                }

                filter_show1.IsVisible = false;
            }
            else
            {
                 await DisplayAlert("Oh Snap!", "Edit the Timesheet after you Connect to Internet", "Ok");
            }
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                var menuitem = sender as MenuItem;
                if (menuitem != null)
                {
                    //for taking the single label value 
                    //  var visit1 = (sender as Xamarin.Forms.MenuItem).BindingContext as string;
                    // DisplayAlert("More Context Action", visit1, "OK");
                    //for taking the items from list
                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;
                    //  var mi = ((MenuItem)sender);
                    if (visit != null)
                    {
                        status = visit.approve_status.ToString();
                        if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                        {
                            await DisplayAlert("Oh Snap!", "Your are not able to Delete, already your Timesheet is " + status, "Ok");
                            menuitem.IsDestructive = true;
                        }
                        if (status.Equals("Rejected") || status.Equals("Saved"))
                        {
                            if (status.Equals("Rejected") || status.Equals("Saved"))
                            {
                                bool del = await DisplayAlert("Confirm?", "Would you like to Delete the Timesheet", "Ok", "Cancel");
                                if (del == true)
                                {
                                    var client = new HttpClient();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                    id = visit.id.ToString();
                                    var postData = new List<KeyValuePair<string, string>>();
                                    postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                                    var content = new FormUrlEncodedContent(postData);
                                    var response = await client.PostAsync(Constants.delete_timesheet, content);
                                    var JsonResult = response.Content.ReadAsStringAsync().Result;
                                    await DisplayAlert("Success!", "Thank you!! " + JsonResult, "Ok");
                                    LoadData();
                                }
                                else
                                {
                                    return;
                                }

                            }
                            if (status.Equals("Approved"))
                            {
                                return;
                            }


                        }

                    }
                }
                filter_show1.IsVisible = false;

            }
            else
            {
                await DisplayAlert("Oh Snap!", "Delete the Timesheet after you Connect to Internet", "Ok");
            }
        }
        protected override bool OnBackButtonPressed()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


                bool getval = Helpers.Settings.DisplaySwitchstatus;
                if (locator.IsGeolocationEnabled && getval == true)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                    }
                    Helpers.Settings.DisplayExitstatus = false;
                    // return Helpers.Settings.DisplayExitstatus;
                }
                else
                {

                    Helpers.Settings.DisplayExitstatus = true;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                        if (await result)
                        {
                            Helpers.Settings.DisplayLoginstatus = true;
                            await Navigation.PushAsync(new Report_injury());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                        }
                        else
                        {
                            string empname;
                            if (App.Current.Properties.ContainsKey("employeename"))
                            {
                                empname = (string)App.Current.Properties["employeename"];
                                Helpers.Settings.DisplayLoginstatus = true;
                                var a = Helpers.Settings.DisplayLoginstatus;
                                // await  DisplayAlert("Empname and status", empname+a, "ok");
                            }

                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");

                            if (Device.OS == TargetPlatform.Android)

                                DependencyService.Get<IAndroidMethods>().CloseApp();

                            //    // base.OnBackButtonPressed();
                            //
                        }

                    });

                }
                base.OnBackButtonPressed();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("************************************************");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("************************************************");
            }
            System.Diagnostics.Debug.WriteLine("************************************************");
            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
            System.Diagnostics.Debug.WriteLine("************************************************");
            return true;
        }

        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
        private void isfiltering(object sender, EventArgs e)
        {
            //filter2.IsVisible = false;

            status2.IsVisible = true;

        }
    }
}