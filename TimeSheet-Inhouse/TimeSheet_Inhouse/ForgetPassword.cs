﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TimeSheet_Inhouse
{
    public class ForgetPassword : ContentPage
    {
        private LoginDesign email_forgetpassword;





        public ForgetPassword()
        {


            email_forgetpassword = new LoginDesign("emailout", "Email Address");
            email_forgetpassword.Padding = new Thickness(25, 73, 25, 0);

            // {
            // Placeholder = "E-Mail",
            //};

            var sumbit = new Button
            {
                Text = "Sumbit",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                TextColor = Color.FromHex("#1A52BA"),
                BackgroundColor = Color.Transparent,


            };
            sumbit.Clicked += (object sender, EventArgs e) => {
               
            };


            Content = new StackLayout
            {
                Children = { email_forgetpassword, sumbit }
            };
        }
    }
}
