﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Approve : TabbedPage
    {
        public Approve()
        {
            InitializeComponent();
            Title = "Approve";

            // this.Children.Add((new Approve_Timesheet() { Icon = "Report.png", Title = "Timesheet" }));
            // this.Children.Add(new NavigationPage(new Approve_Timesheet()) { Icon = "timersmall.png", Title = "Timesheet" });
            //Children.Add(new Approve_Timesheet());
           // this.Children.Add((new Approve_Leave() { Icon = "leave.png", Title = "Leave" }));
            this.Children.Add((new Timesheet_Approve() { Icon = "Report.png", Title = "Timesheet" }));
            this.Children.Add((new Timesheet_Leave() { Icon = "leave.png", Title = "Leave" }));
           // this.Children.Add((new Approve_Timesheet() { Icon = "timersmall.png", Title = "Timesheet" }));
            // Children.Add(new Approve_Leave());
        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
    }
}