﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;

using System.Net.Http;
using System.Net.Http.Headers;

using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Geolocator;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Timesheet_Approve : ContentPage
    {

        string setempid, user_id, JsonResult, id_reject, reject_desc, setcompid;
        public Timesheet_Approve()
        {
            InitializeComponent();

            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            listview12.IsVisible = false;
            check_connection();
            Loademployee();

            employee.SelectedIndexChanged += (sender, args) =>
            {

                string employeeselected = employee.Items[employee.SelectedIndex];
                string[] id = employeeselected.Split('-');
                foreach (string getid in id)
                {
                    user_id = id[1];

                }
                LoadTimesheet(user_id);
            };


        }

        public async void Loademployee()
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));

                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.get_employee, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                string result = JsonResult.Trim(']').Trim('[');

                string[] tokens = result.Split(',');
                try
                {
                    for (int i = 0; i < tokens.Length; i++)
                    {
                        string a = tokens[i];
                        string b = a.Trim('"').Trim('"');
                        employee.Items.Add(b);
                    }
                }
                catch (Exception e)
                {

                }
            }else
            {
                await DisplayAlert("Oh Snap!", "Employee won't be loaded until you Connect to Internet", "Ok");
            }
           
        }







        public async void LoadTimesheet(string userid)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                string no = "No";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, userid));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.get_pending_timesheet, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                //   string result = JsonResult.Trim(']').Trim('[');
                try
                {
                    if (no.Equals(JsonResult))
                    {
                        no_message.IsVisible = true;
                        no_message.Text = "No Data are available ";
                        listview12.IsVisible = false;
                    }
                    else
                    {
                        var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                        listview12.ItemsSource = rootobject;
                        no_message.IsVisible = false;
                        listview12.IsVisible = true;

                    }
                }
                catch (Exception e)
                {

                }

            }
            else
            {

                await DisplayAlert("Oh Snap!", "Timesheet for an Employee won't be loaded until you Connect to Internet", "Ok");
            }
        }

        private async void Approved(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                try
                {
                    var item = (Xamarin.Forms.Image)sender;
                    if (item != null)
                    {
                        var visit = (sender as Xamarin.Forms.Image).BindingContext as ItemClass;
                        if (visit != null)
                        {
                            string id = visit.id.ToString();
                            approve_timesheet(id);
                        }
                        else
                        {
                          await  DisplayAlert("Oh Snap!", "No value", "Ok");
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else {


                await DisplayAlert("Oh Snap!", "Approve the Timesheet after you Connect to Internet", "Ok");
            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        private async void Rejected(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                try
                {
                    overlay.IsVisible = true;
                    var item = (Xamarin.Forms.Image)sender;
                    if (item != null)
                    {
                        var visit = (sender as Xamarin.Forms.Image).BindingContext as ItemClass;
                        if (visit != null)
                        {

                            id_reject = visit.id.ToString();

                            //Reject_Timesheet(id);
                        }
                        else
                        {
                           //await DisplayAlert("ok", "gfghhj", "ok");
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception ec)
                {

                }

            }
            else {

                await DisplayAlert("Oh Snap!", "Reject the Timesheet after you Connect to Internet", "Ok");

            }
        }

      
        private void onItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (((ListView)sender).SelectedItem != null)
            {
                listview12.SelectedItem = null;
            }
            else
            {

            }
        }

        private void ok_clicked(object sender, EventArgs e)
        {
            string Rejected_id = id_reject;
            reject_desc = EnteredName.Text;

            overlay.IsVisible = false;
            Reject_Timesheet(Rejected_id);
        }

        //private void OnCancelButtonClicked(object sender, EventArgs e)
        //{
        //    overlay.IsVisible = false;
        //}

        //private void OnOKButtonClicked(object sender, EventArgs e)
        //{
        //    overlay.IsVisible = false;
        //    DisplayAlert("Result", "okay", "OK");
        //}

        private async void approve_timesheet(string id)
        {

            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));
            postData.Add(new KeyValuePair<string, string>("approved_by", setempid));

            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Approve_timesheet, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "Ok");
            LoadTimesheet(user_id);


            Enter_timesheet ent = new Enter_timesheet();
            ent.LoadTimesheet();

        }
        private async void Reject_Timesheet(string id)
        {

            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));
            postData.Add(new KeyValuePair<string, string>("approved_by", setempid));
            postData.Add(new KeyValuePair<string, string>("rejected_desc", reject_desc));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Reject_timesheet, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "Ok");
            LoadTimesheet(user_id);

        }
        protected override bool OnBackButtonPressed()
        {
            var locator = CrossGeolocator.Current;
            //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


            bool getval = Helpers.Settings.DisplaySwitchstatus;
            if (locator.IsGeolocationEnabled && getval == true)
            {
                if (Device.OS == TargetPlatform.Android)
                {
                    DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                }
                Helpers.Settings.DisplayExitstatus = false;
               // return Helpers.Settings.DisplayExitstatus;
            }
            else
            {

                Helpers.Settings.DisplayExitstatus = true;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                    if (await result)
                    {
                        Helpers.Settings.DisplayLoginstatus = true;
                        await Navigation.PushAsync(new Report_injury());
                    }
                    else
                    {
                        string empname;
                        if (App.Current.Properties.ContainsKey("employeename"))
                        {
                            empname = (string)App.Current.Properties["employeename"];
                            Helpers.Settings.DisplayLoginstatus = true;
                            var a = Helpers.Settings.DisplayLoginstatus;
                            // await  DisplayAlert("Empname and status", empname+a, "ok");
                        }



                        if (Device.OS == TargetPlatform.Android)

                            DependencyService.Get<IAndroidMethods>().CloseApp();

                        //    // base.OnBackButtonPressed();
                        //
                    }

                });
                return Helpers.Settings.DisplayExitstatus;
            }


            return true;




        }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
