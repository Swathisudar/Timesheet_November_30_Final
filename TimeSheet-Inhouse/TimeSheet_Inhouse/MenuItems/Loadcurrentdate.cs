﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class Loadcurrentdate
    {



        string dateformat;
        public Loadcurrentdate()
        {
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
        }

        public Loadcurrentdate(DateTime Current)

        {
           
            this.Current = Current;
           
        }

        public DateTime Current { set; get; }
        public string Datestring
        {
            get
            {

                return Current.ToString(dateformat);
            }
        }
        public DateTime Next { private set; get; }
    }
}
