﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using TimeSheet_Inhouse.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeSheet_Inhouse.Helpers;
using Plugin.Geolocator.Abstractions;
using System.Net.Http.Headers;
using Plugin.Geolocator;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace TimeSheet_Inhouse
{
    public partial class Enter_timesheet : MasterDetailPage
    {
        public List<MasterPageItem> menuList { get; set; }

        string emp_name, b = "", c = "Approve", setcompid, setempid, JsonResult;
        int n;
        bool swtich_status;//a;
        Boolean getval;
        public int[] tokens;
        int sav, sub, appr, rej,count=1;
        string comp_id_role;
        public Enter_timesheet()
        {
            check_connection();
            LoadTimesheet();
          
        }
        //MasterPageItem page1,page2,page3,page4,page5, page6,page7,page8;
        public Enter_timesheet(string employeename, string companyid, string employeeid, string email, string acess_approve_value)
        {


            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();
        
            check_connection();
            //App.IsLoggedIn = true;
            logout_btn.IsEnabled = false;

            Helpers.Settings.DisplayLoginstatus = true;

            setempid = Helpers.Settings.Displayemployeeid;
            setcompid = Helpers.Settings.Displaycompanyid;
            MessagingCenter.Subscribe<HomePage, string>(this, "companyid", (sender, arg) =>
            {
                comp_id_role = arg;
                App.Current.Properties["companyid"] = comp_id_role;
              //  Helpers.Settings.Displaycompanyid = comp_id_role;


            });




            App.Current.Properties["employeename"] = employeename;

            App.Current.Properties["employeeid"] = employeeid;
            // App.Current.Properties["companyname"] = company_name;
            App.Current.Properties["email"] = email;

            App.Current.Properties["controlaccess"] = acess_approve_value;


            System.Diagnostics.Debug.WriteLine("^^^^^^^^^^^^^^^^^^ -EnterTimesheet                            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

            System.Diagnostics.Debug.WriteLine("GettingData:" + employeename+ companyid+ employeeid+ acess_approve_value+ email);
            //
            System.Diagnostics.Debug.WriteLine("Company id" + companyid);
            System.Diagnostics.Debug.WriteLine("Email" + email);
            System.Diagnostics.Debug.WriteLine("empname" + employeename);
            System.Diagnostics.Debug.WriteLine("Employee_id" + employeeid);
            System.Diagnostics.Debug.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");


            App.Current.SavePropertiesAsync();

            // getrole(companyid, email);
            //  DisplayAlert("CompanyPass", (string)App.Current.Properties["companyid"], "okay");

            getemployeerole();
            getactive_status();
           // setempid = employeeid;
            //setcompid = companyid;
            logout_btn.IsEnabled = true;
            logout_btn.TextColor = Color.White;
            Loaddetails();

            MessagingCenter.Subscribe<HomePage, string>(this, "emplyeename", (sender, arg) =>
            {
                emp_name = arg;
                empname.Text = "Hi " + emp_name;
            });
            empname.Text = "Hi " + employeename;
            empid.Text = setempid;



            getval = Helpers.Settings.DisplaySwitchstatus;


            if (getval == true)
            {
                location.IsToggled = true;
                locationlabel.Text = "Tracking... ON";
                if (n == 0)
                {
                    //DisplayAlert("ok", "hijhkjhkj", "ok");
                }
                else
                {
                    ////TODO: TEST CODE
                    #region LOCATION PART

                    ////TODO: FAKE:
                    MessagingCenter.Send(new Message(true, n), string.Empty);
                    //return;

                    #endregion
                }
            }
            else
            {
                location.IsToggled = false;
                locationlabel.Text = "Tracking... OFF";
                
            }


            //   getdate();
            menuList = new List<MasterPageItem>();
                // Creating our pages for menu navigation
                // Here you can define title for item, 
                // icon on the left side, and page that you want to open after selection
                var page1 = new MasterPageItem() { Title = "Day Timesheet", Icon = "daytimesheetone.png", TargetType = typeof(DayTimesheet) };
                var page2 = new MasterPageItem() { Title = "Weekly Timesheet", Icon = "weektimesheet.png", TargetType = typeof(Weekly_Timesheet) };
                var page3 = new MasterPageItem() { Title = "View the Timesheet", Icon = "view.png", TargetType = typeof(View_Weekly_Timesheet) };
                //var page4 = new MasterPageItem() { Title = "Saved Timesheet", Icon = "savedtimesheetsmall.png", TargetType = typeof(View_Saved_Timesheet) };
                var page5 = new MasterPageItem() { Title = "Check in/out", Icon = "checkinout.png", TargetType = typeof(CheckInOut) };
                var page6 = new MasterPageItem() { Title = "Injury Report", Icon = "assign.png", TargetType = typeof(Report_injury) };
                var page7 = new MasterPageItem() { Title = "Help", Icon = "helpc.png", TargetType = typeof(help) };
                //   var page8 = new MasterPageItem() { Title = "Logout", Icon = "logout.png", TargetType = typeof(Logedout) };
                //var page8 = new MasterPageItem() { Title = "Logout", Icon = "logout.png", TargetType = typeof(ReportInjury_Logout) };
                var page9 = new MasterPageItem() { Title = "Approve", Icon = "approve.png", TargetType = typeof(Approve) };

                var page10 = new MasterPageItem() { Title = "Chart", Icon = "chart.png", TargetType = typeof(chart) };
                if (acess_approve_value == "admin")
                {
                    menuList.Add(page10);
                    menuList.Add(page7);
                    //  menuList.Add(page8);

                    navigationDrawerList.ItemsSource = menuList;
                    // Initial navigation, this can be used for our home page
                    Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(help)));
                }


                //--------------------------------    comment for checking----------------------
                 else if (acess_approve_value == "manager")
                {
                // Adding menu items to menuList
                menuList.Add(page5);
                menuList.Add(page9);

                menuList.Add(page7);
                // menuList.Add(page8);



                navigationDrawerList.ItemsSource = menuList;
                // Initial navigation, this can be used for our home page
                //Detail = new NavigationPage();
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(Approve)));
            }



            else if (acess_approve_value == "user")
                {


                    menuList.Add(page1);
                    menuList.Add(page2);
                    menuList.Add(page3);
                    // menuList.Add(page4);
                    menuList.Add(page5);
                    menuList.Add(page6);
                    menuList.Add(page7);

                    // menuList.Add(page8);

                    navigationDrawerList.ItemsSource = menuList;
                    // Initial navigation, this can be used for our home page
                    //Detail = new NavigationPage();
                    Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(DayTimesheet)));
                }
            else if (acess_approve_value == "admin_approve")
            {


                menuList.Add(page5);
                menuList.Add(page9);
                menuList.Add(page10);
                menuList.Add(page7);
                // menuList.Add(page8);
                navigationDrawerList.ItemsSource = menuList;
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(Approve)));

            }
            else if (acess_approve_value == "admin_timesheet")
            {



                menuList.Add(page1);
                menuList.Add(page2);
                menuList.Add(page3);
                //   menuList.Add(page4);
                menuList.Add(page5);
                menuList.Add(page6);
                menuList.Add(page10);
                menuList.Add(page7);

                //  menuList.Add(page8);
                navigationDrawerList.ItemsSource = menuList;
                // Initial navigation, this can be used for our home page
                //Detail = new NavigationPage();
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(DayTimesheet)));



            }
            else if (acess_approve_value == "approve_timesheet")
            {

                menuList.Add(page1);
                menuList.Add(page2);
                menuList.Add(page3);
                //  menuList.Add(page4);
                menuList.Add(page5);
                menuList.Add(page6);
                menuList.Add(page9);
                menuList.Add(page7);

                //  menuList.Add(page8);
                navigationDrawerList.ItemsSource = menuList;
                // Initial navigation, this can be used for our home page
                //Detail = new NavigationPage();
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(DayTimesheet)));




            }
            //   else if (access_control == "admin_approve_timesheet")




            // //--------------------------------    comment for checking----------------------
            else if (acess_approve_value == "all")
                {

                    menuList.Add(page1);
                    menuList.Add(page2);
                    menuList.Add(page3);
                    //   menuList.Add(page4);
                    menuList.Add(page5);
                    menuList.Add(page6);
                    menuList.Add(page9);
                    menuList.Add(page10);
                    menuList.Add(page7);

                    //   menuList.Add(page8);
                    navigationDrawerList.ItemsSource = menuList;
                    // Initial navigation, this can be used for our home page
                    //Detail = new NavigationPage();
                    Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(DayTimesheet)));




                }

                else if(acess_approve_value== "none")
            {
                    menuList.Add(page5);
                    menuList.Add(page7);

                    //   menuList.Add(page8);

                    navigationDrawerList.ItemsSource = menuList;
                    // Initial navigation, this can be used for our home page
                    //Detail = new NavigationPage();
                    Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(help)));









                }
            else
            {
                menuList.Add(page5);
                menuList.Add(page7);

                //   menuList.Add(page8);

                navigationDrawerList.ItemsSource = menuList;
                // Initial navigation, this can be used for our home page
                //Detail = new NavigationPage();
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(help)));
            }
                // var locator = CrossGeolocator.Current;
                // if (!locator.IsGeolocationEnabled)
                // {
                //  location.IsToggled = false;

                // }

                location.Toggled += location_toggled;



            

        }
        public void OnLogOut(object sender, EventArgs args)
        {
            // App.IsLoggedIn = false;
            // logout_method();

            //if (App.Current.Properties.ContainsKey("employeename"))
            //// App.Current.Properties.ContainsKey("employeeid") && App.Current.Properties.ContainsKey("acesscontrol") && App.Current.Properties.ContainsKey("companyid"))
            //{
            //    App.Current.Properties.Clear();
            //    //Helpers.Settings.DisplayLoginstatus = false;
            //    //DisplayAlert("Datas:", "employeename:" + App.Current.Properties["employeename"],"ok");
            //    // Navigation.PopToRootAsync();

            //    logout_btn.IsEnabled = false;
               
            //}
            //else
            //{
            //    return;
            //   // logout_btn.IsEnabled = true;
            //   // App.Current.MainPage = new NavigationPage(new HomePage());
            //}
            //  App.Current.MainPage = new NavigationPage(new HomePage());
            Helpers.Settings.DisplayLoginstatus = false;
            Helpers.Settings.DisplayEmployeename = null;
            // App app = App.Current as App;
            //  App.Current.MainPage = new HomePage();

            //App.Current.MainPage = new NavigationPage(new HomePage());
            Navigation.PushAsync(new HomePage());
        }


        private void location_toggled(object sender, ToggledEventArgs e)
        {
            count=count + 1;
            var locator = CrossGeolocator.Current;
            //Cross Geolocator.Current is used for currently gps is ON state or OFF state 
            if (!locator.IsGeolocationEnabled)
            {
                if (count % 2 == 0)
                {
                    try
                    {
                        ////TODO: TEST CODE
                        #region LOCATION PART

                        ////TODO: FAKE:
                        MessagingCenter.Send(new Message(true, n), string.Empty);
                        //return;
                        #endregion
                    }
                    catch (Exception ex) {
                        DisplayAlert("Alert!", "Please Turn on your Location for Track OFF", "Ok");
                    }



                    System.Diagnostics.Debug.WriteLine("*****If Part************"+count+"****************");
                    //DisplayAlert("If Part", count.ToString(), "ok");
                }
                else
                {

                    System.Diagnostics.Debug.WriteLine("*****Else Part************" + count + "****************");
                   // DisplayAlert("Else Part", count.ToString(), "ok");
                }



                //getval button status

                if (getval == true)
                {
                    location.IsToggled = true;
                    locationlabel.Text = "Tracking... ON";
                }
                else
                {

                    
                
                    location.IsToggled = false;
                    locationlabel.Text = "Tracking... OFF";
                   

                }
                
            }
            
            else
            {
                swtich_status = e.Value;
                Helpers.Settings.DisplaySwitchstatus = e.Value;
                //DisplayAlert("ok", Helpers.Settings.DisplaySwitchstatus.ToString(), "ok");
                if (swtich_status == true)
                {

                    locationlabel.Text = "Tracking... ON";
                    if (n == 0)
                    {
                        DisplayAlert("Oh Snap", "your are not under Tracking ", "Ok");
                    }
                    else
                    {
                        ////TODO: TEST CODE
                        #region LOCATION PART

                        ////TODO: FAKE:
                        MessagingCenter.Send(new Message(true,n), string.Empty);
                    //return;

                        #endregion


                        //DisplayAlert("Oh Snap", "Tracking ON ", "ok");
                    }


                }

                else if (swtich_status == false)
                {
                    locationlabel.Text = "Tracking... OFF";
                    if (n == 0)
                    {
                        DisplayAlert("Oh Snap", "your are not under Tracking ", "Ok");
                    }
                    else
                    {
                        try
                        {
                            ////TODO: TEST CODE
                            #region LOCATION PART
                            ////TODO: FAKE:
                            MessagingCenter.Send(new Message(false, n), string.Empty);
                            //return;
                            #endregion
                        }
                        catch (Exception ex)
                        {
                           
                        }
                    }

                }
                else
                {

                }
            }
        }

        private async void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                var item = (MasterPageItem)e.SelectedItem;
                Type page = item.TargetType;


                Detail = new NavigationPage((Page)Activator.CreateInstance(page));
                string val = item.Title.ToString();

                IsPresented = false;
            }
            else {
                await DisplayAlert("Oh Snap!", "You can't able to Navigate. Please Check your Internet Connection", "Ok");
            }
                  }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
        ////TEST CODE
        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<Position>(this, string.Empty, OnLocationUpdate); //One
        }
        private async void OnLocationUpdate(Position position)
        {
            bool status= Helpers.Settings.DisplaySwitchstatus;
            if (status == true) {
                var client = new HttpClient();
                //Send to server

                Helpers.Settings.DisplaySwitchstatus = true;
                System.Diagnostics.Debug.WriteLine(nameof(OnLocationUpdate) + " Location " + position.Latitude + " " + position.Longitude);

                string lat = position.Latitude.ToString();
                string longi = position.Longitude.ToString();

                var clientone = new System.Net.Http.HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postDataone = new List<KeyValuePair<string, string>>();

                if (string.IsNullOrEmpty(setempid))
                {
                    setempid = Helpers.Settings.Displayemployeeid;
                }
                postDataone.Add(new KeyValuePair<string, string>("user_id", setempid));
                postDataone.Add(new KeyValuePair<string, string>("event_id", "1"));
                // postData.Add(new KeyValuePair<string, string>("notes", getnotes));
                postDataone.Add(new KeyValuePair<string, string>(Constants.latitude, lat));
                postDataone.Add(new KeyValuePair<string, string>(Constants.longitude, longi));
                // postData.Add(new KeyValuePair<string, string>("parent_id", eve));

                var contentone = new System.Net.Http.FormUrlEncodedContent(postDataone);
                var responseone = await client.PostAsync(Constants.location_update, contentone);
                //to take the value direstly without the array 
                JsonResult = responseone.Content.ReadAsStringAsync().Result;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("*********************************NO VALUE **********************************");
            }
               
            
        }
        public async void Loaddetails()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            //  postData.Add(new KeyValuePair<string, string>("name", "bkimbmn"));
            //setcompid = Helpers.Settings.Displaycompanyid;
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
          //  await DisplayAlert("LoadDetailsMethod for logo  company id", setcompid, "okay");



            postData.Add(new KeyValuePair<string, string>("company_id", setcompid));


            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_company_settings, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            //take the single element without the array can be used by this below line
            //  var rootobject = JsonConvert.DeserializeObject<Person>(JsonResult);
            //take  the single element with the array can be used by this below line
            // this is to take the values from the array it trim the array holder from the json result
            string result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Login_user_Details>(result.ToString());
            //to load the image from the database
            string logo = rootobject.logo;
            //company_logo

            company_logo.Source = ImageSource.FromUri(new Uri(logo));
            n = Int32.Parse(rootobject.time_track);
            if (rootobject.date_formats == "dd-mm-yy")
            {
                App.Current.Properties["Dateformat"] = "dd-MM-yyyy";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "dd-MM-yyyy");
            }
            else if (rootobject.date_formats == "dd/mm/yy")
            {
                App.Current.Properties["Dateformat"] = "dd/MM/yyyy";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "dd/MM/yyyy");
            }
            else if (rootobject.date_formats == "mm-dd-yy")
            {
                App.Current.Properties["Dateformat"] = "MM-dd-yyyy";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "MM-dd-yyyy");
            }
            else if (rootobject.date_formats == "mm/dd/yy")
            {
                App.Current.Properties["Dateformat"] = "MM/dd/yyyy";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "MM/dd/yyyy");
            }
            else if (rootobject.date_formats == "yy-mm-dd")
            {
                App.Current.Properties["Dateformat"] = "yyyy-MM-dd";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "yyyy-MM-dd");
            }
            else if (rootobject.date_formats == "yy/mm/dd")
            {
                App.Current.Properties["Dateformat"] = "yyyy/MM/dd";
                MessagingCenter.Send<Enter_timesheet, string>(this, "Dateformat", "yyyy/MM/dd");
            }
            else
            {

            }

        }
     
        public async void getemployeerole()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
         //   setcompid = Helpers.Settings.Displaycompanyid;
         
          //  string comp_id = (string)App.Current.Properties["companyid"];





            string comp_id = Helpers.Settings.Displaycompanyid;
            string email = Helpers.Settings.Displayemail;
            string access_control = (string)App.Current.Properties["controlaccess"];
           // await DisplayAlert("getemployeerole company id, email,accesscontrol", comp_id+" "+email+""+access_control, "okay");
            System.Diagnostics.Debug.WriteLine("*********&&&&&&&&& Entertimesheet Page getemployee role method-   &&&&&&&&&&&&&&&&&&&&&&&**************");
            System.Diagnostics.Debug.WriteLine("Company id: " + comp_id);

            System.Diagnostics.Debug.WriteLine("email: " + email);


            postData.Add(new KeyValuePair<string, string>("company_id", comp_id));
            postData.Add(new KeyValuePair<string, string>("email", email));

            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_role_url, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            System.Diagnostics.Debug.WriteLine("get role Jsonresult " + JsonResult);
            //JsonResult = "user";
            // App.Current.Properties["controlaccess1"] = JsonResult;
            //string acc2 = JsonResult;

           // App.Current.Properties["accessing"] = acc2;
          //  string acc_prop1 = (string)App.Current.Properties["accessing"];

           //await DisplayAlert("acc_essloc set", " acce" +acc_prop1+ ",", "okay");

            Helpers.Settings.DisplayAccesscontrolstatus = JsonResult;
          // await  App.Current.SavePropertiesAsync();
          //if(access_control == acc2)
          //  {

          //  }
          //else
          //  {

          //  }

        }

        public async void getactive_status()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            //   setcompid = Helpers.Settings.Displaycompanyid;

            //  string comp_id = (string)App.Current.Properties["companyid"];





           // string comp_id = Helpers.Settings.Displaycompanyid;
            string email = Helpers.Settings.Displayemail;
           // string access_control = (string)App.Current.Properties["controlaccess"];
           // await DisplayAlert("geactive status", email , "okay");
            System.Diagnostics.Debug.WriteLine("*********&&&&&&&&& Entertimesheet Page getactive status  method-   &&&&&&&&&&&&&&&&&&&&&&&**************");
           

            System.Diagnostics.Debug.WriteLine("email: " + email);


           // postData.Add(new KeyValuePair<string, string>("company_id", comp_id));
            postData.Add(new KeyValuePair<string, string>("email", email));

            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_active_status, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            System.Diagnostics.Debug.WriteLine(" Active status Jsonresult " + JsonResult);
            //JsonResult = "user";
            // App.Current.Properties["controlaccess1"] = JsonResult;
            //string acc2 = JsonResult;

            // App.Current.Properties["accessing"] = acc2;
            //  string acc_prop1 = (string)App.Current.Properties["accessing"];

            //await DisplayAlert("acc_essloc set", " acce" +acc_prop1+ ",", "okay");

            Helpers.Settings.DisplayActiveEmailstatus = JsonResult;
            // await  App.Current.SavePropertiesAsync();
            //if(access_control == acc2)
            //  {

            //  }
            //else
            //  {

            //  }

        }


        public async void LoadTimesheet()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            // postData.Add(new KeyValuePair<string, string>(Constants.company_id, "keranmekran"));

            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_total_timesheet, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');

            tokens = result.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            sav = tokens[0];
            sub = tokens[1];
            appr = tokens[2];
            rej = tokens[3];
            App.Current.Properties["save_t"] = sav;
            App.Current.Properties["sub_t"] = sub;
            App.Current.Properties["appr_t"] = appr;
            App.Current.Properties["rej_t"] = rej;
        }


       

    }
}
