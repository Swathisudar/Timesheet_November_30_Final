﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Current_Week : ContentPage
    {
        string emp_id, projectname, taskname, id, date, hours, status;
        private bool isRowEven;
        MenuItem menuitem;
        string hrs_value, min_value, dateformat, setcompid;
       
        int PreviousItemId = 0;
        string direction = "none";
        
       

        private async void Submit_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var menuitem = sender as MenuItem;
                if (menuitem != null)
                {

                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                    if (visit != null)
                    {
                        try
                        {
                            status = visit.approve_status.ToString();
                            if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                            {

                                await DisplayAlert("Oh Snap!", "Your are not able to Submit, already your Timesheet is " + status, "Ok");
                                menuitem.IsDestructive = true;
                            }
                            if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                            {
                                var client = new HttpClient();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                string id = visit.id.ToString();
                                var postData = new List<KeyValuePair<string, string>>();
                                postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                                var content = new FormUrlEncodedContent(postData);
                                var response = await client.PostAsync(Constants.submit_saved_timesheet, content);
                                var JsonResult = response.Content.ReadAsStringAsync().Result;
                                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "Ok");
                                filter_show1.IsVisible = false;
                                // filter_show1.Text = "All";
                                listview1.IsVisible = false;
                                listview1.ItemTapped += OnItemTapped;

                                LoadData();
                            }
                            else
                            {

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            else
            {
                await DisplayAlert("Oh Snap!", "Submit the Timesheet after you Connect to Internet", "Ok");
            }
        }
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (((ListView)sender).SelectedItem != null)
            {
                listview1.SelectedItem = null;
            }else
            {

            }
        }

        //private bool _isRefreshing = false;
        //public bool IsRefreshing
        //{
        //    get { return _isRefreshing; }
        //    set
        //    {
        //        _isRefreshing = value;
        //        OnPropertyChanged(nameof(IsRefreshing));
        //    }
        //}
        //public ICommand RefreshCommand
        //{
        //    get
        //    {
        //        return new Command(() =>
        //        {
        //            IsRefreshing = true;



        //            IsRefreshing = false;
        //        });
        //    }
        //}
        public Current_Week()
        {
            InitializeComponent();
            this.Title = "Current Week";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                emp_id = (string)App.Current.Properties["employeeid"];
           
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];


         
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            //DisplayAlert("Currentweek companyid", setcompid, "okay");
            MessagingCenter.Subscribe<Edit_Timesheet>(this, "MyItemsChanged", sender => {
                LoadData();
            });
            filter_show1.IsVisible = false;
            LoadData();
            
          

            listview1.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {

               // ((AbsoluteLayout)e.SelectedItem).BackgroundColor = Color.AntiqueWhite;
               
            };
            

        }

        private async void ImageClicked(object sender, EventArgs e)
        {
            //overlay.IsVisible = true;
            string statusselected;
            var action = await DisplayActionSheet("Filter the Timesheet?", "Cancel", null, "Submitted", "Saved", "Approved", "Rejected", "All");
            switch (action)
            {
                case "Submitted":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Saved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Approved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Rejected":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "All":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;

            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        public async void LoadData()
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                //filter_show1.IsVisible = false;
                string no = "No";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(emp_id))
                {
                    emp_id = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }

                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Current_week_timesheet, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                indicator.IsRunning = false;
                indicator.IsVisible = false;
                //   string result = JsonResult.Trim(']').Trim('[');
                if (no.Equals(JsonResult))
                {
                    no_message.IsVisible = true;

                    no_message.Text = "No Timesheet are available";
                    // no_message.TextColor = Color.Black;
                    listview1.IsVisible = false;
                }
                else
                {
                    var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                    listview1.IsVisible = true;
                    no_message.IsVisible = false;
                    listview1.ItemTapped += OnItemTapped;
                    listview1.ItemsSource = rootobject;
                    listview1.ItemAppearing += (object sender, ItemVisibilityEventArgs e) =>
                      {


                      };

                }

            }
            else
            {
                await DisplayAlert("Oh Snap!", "View the Timesheet after you Connect to Internet", "Ok");
            }
        }



    
        public async void filter_LoadData(string sel_status)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                if (sel_status == "All")
                {
                    filter_show1.IsVisible = true;
                    filter_show1.Text = "All";
                    LoadData();

                }
                else
                {

                    string no = "No";
                    filter_show1.IsVisible = true;
                    filter_show1.Text = sel_status;
                    string status_sel = sel_status;
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                    var postData = new List<KeyValuePair<string, string>>();
                    if (string.IsNullOrEmpty(emp_id))
                    {
                        emp_id = Helpers.Settings.Displayemployeeid;
                    }
                    if (string.IsNullOrEmpty(setcompid))
                    {
                        setcompid = Helpers.Settings.Displaycompanyid;
                    }

                    postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                    postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                    postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                    var content = new FormUrlEncodedContent(postData);
                    var response = await client.PostAsync(Constants.Filter_thisweek_status, content);
                    var JsonResult = response.Content.ReadAsStringAsync().Result;
                    if (no.Equals(JsonResult))
                    {
                        no_message.IsVisible = true;

                        no_message.Text = "No Timesheet are available";
                        //   no_message.TextColor = Color.Black;
                        listview1.IsVisible = false;
                    }
                    else
                    {
                        string result = JsonResult.Trim(']').Trim('[');
                        var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                        no_message.IsVisible = false;
                        listview1.IsVisible = true;
                        listview1.ItemTapped += OnItemTapped;
                        listview1.ItemsSource = rootobject;
                        overlay.IsVisible = false;
                    }
                }
            }
            else
            {
                await DisplayAlert("Oh Snap!", "View the Filtered Timesheet after you Connect to Internet", "Ok");
            }
        }

        private async void Edit_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {

                var menuitem = sender as MenuItem;

                if (menuitem != null)
                {


                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;


                    if (visit != null)
                    {

                        projectname = visit.project_name.ToString();
                        taskname = visit.task_name.ToString();
                        id = visit.id.ToString();
                        //   date = visit.date.ToString("MM - dd - yyyy");
                        date = visit.date.ToString();
                        hours = visit.Hourstring.ToString();

                        status = visit.approve_status.ToString();
                        if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                        {
                           await  DisplayAlert("Oh Snap!", "Your are not able to Edit, already your Timesheet is " + status, "Ok");
                            menuitem.IsDestructive = true;
                        }
                        if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                        {
                            string[] hrs = hours.Split(':');
                            for (int i = 0; i < hrs.Length; i++)
                            {
                                hrs_value = hrs[0];
                                min_value = hrs[1];

                            }
                           await Navigation.PushAsync(new Edit_Timesheet(projectname, taskname, id, hrs_value, min_value, date));
                        }
                        if (status.Equals("Approved"))
                        {
                            return;
                        }

                    }




                }
                else
                {
                    return;
                }



                filter_show1.IsVisible = false;
            }else
            {
                await DisplayAlert("Oh Snap!", "Edit the Timesheet after you Connect to Internet", "Ok");
            }

        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var menuitem = sender as MenuItem;

                if (menuitem != null)
                {

                    var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                    if (visit != null)
                    {

                        status = visit.approve_status.ToString();
                        if ((status.Equals(Constants.approvestatus_submitted)) || (status.Equals(Constants.approvestatus_approved)))
                        {
                            await DisplayAlert("Oh Snap!", "Your are not able to Delete, already your Timesheet is " + status, "Ok");
                            menuitem.IsDestructive = true;
                        }
                        if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                        {
                            bool del = await DisplayAlert("Confirm?", "Would you like to Delete the Timesheet", "Ok", "Cancel");
                            if (del == true)
                            {
                                var client = new HttpClient();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                id = visit.id.ToString();
                                var postData = new List<KeyValuePair<string, string>>();
                                postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                                var content = new FormUrlEncodedContent(postData);
                                var response = await client.PostAsync(Constants.delete_timesheet, content);
                                var JsonResult = response.Content.ReadAsStringAsync().Result;
                                await DisplayAlert("Alert!", JsonResult, "Ok");
                                LoadData();
                            }
                            else
                            {
                                return;
                            }
                        }
                        if (status.Equals("Approved"))
                        {
                            return;
                        }
                    }

                }
                filter_show1.IsVisible = false;
            }
            else
            {
                await DisplayAlert("Oh Snap!", "Delete the Timesheet after you Connect to Internet", "Ok");
            }
       
        }
        private void Cell_OnAppearing(object sender, EventArgs e)
        {
            if (this.isRowEven)
            {
                var viewCell = (ViewCell)sender;

                if (viewCell.View != null)
                {
                    viewCell.View.BackgroundColor = Color.Beige;
                }
            }

            this.isRowEven = !this.isRowEven;
        }
        //public void OnitemClicked(object sender, SelectedItemChangedEventArgs e)
        //{

        //    var obj = (sender as ListView).SelectedItem as ItemClass;
        //    obj.itemcolor = Color.Blue;
        //}

        protected override bool OnBackButtonPressed()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


                bool getval = Helpers.Settings.DisplaySwitchstatus;
                if (locator.IsGeolocationEnabled && getval == true)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                    }
                    Helpers.Settings.DisplayExitstatus = false;
                    // return Helpers.Settings.DisplayExitstatus;
                }
                else
                {

                    Helpers.Settings.DisplayExitstatus = true;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                        if (await result)
                        {
                            Helpers.Settings.DisplayLoginstatus = true;
                            await Navigation.PushAsync(new Report_injury());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                        }
                        else
                        {
                            string empname;
                            if (App.Current.Properties.ContainsKey("employeename"))
                            {
                                empname = (string)App.Current.Properties["employeename"];
                                Helpers.Settings.DisplayLoginstatus = true;
                                var a = Helpers.Settings.DisplayLoginstatus;
                                // await  DisplayAlert("Empname and status", empname+a, "ok");
                            }

                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");

                            if (Device.OS == TargetPlatform.Android)

                                DependencyService.Get<IAndroidMethods>().CloseApp();

                            //    // base.OnBackButtonPressed();
                            //
                        }

                    });

                }
                base.OnBackButtonPressed();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("************************************************");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("************************************************");
            }
            System.Diagnostics.Debug.WriteLine("************************************************");
            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
            System.Diagnostics.Debug.WriteLine("************************************************");
            return true;
        }

        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
        //private void isfiltering(object sender, EventArgs e)
        //{
        //    //filter1.IsVisible = false;

        //    status1.IsVisible = true;

        //}
    }
}