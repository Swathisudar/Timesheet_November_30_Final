﻿using Java.Util;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Weekly_Timesheet : ContentPage, IElementConfiguration<Frame>
    {
        Enter_timesheet ent = new Enter_timesheet();
        public Label header,  showdate, get_description, localpath_label, heading;
        public Picker picker, task;

        string seconds_covert, dateformat, setcompid, setempid, project_select, task_select, date, desc, min_value, hrs_value, time;
        DatePicker date_picker;
       

        private Entry stime, description;



        public Weekly_Timesheet()
        {
            //InitializeComponent();
            this.Title = "Weekly Timesheet";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
         
           // DisplayAlert("Prevois week companyid", setcompid, "okay");
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];

            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            getcurrentdate();

            //getdate();
            header = new Label
            {
                Text = "Project Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Label header2 = new Label
            {
                Text = "Task Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Label header3 = new Label
            {
                Text = "Duration",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),

            };
            Label curdatetxt = new Label
            {
                Text = "Date",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            
          
            date_picker = new DatePicker()
            {
                Format = dateformat,
                HorizontalOptions = LayoutOptions.End

            };

            description = new Entry
            {
                Placeholder = "Write a Task Description"
            };


            stime = new Entry
            {
                Placeholder = "00.00 hrs",
                //to set the keyboard only for Number
                Keyboard = Keyboard.Numeric
            };
            Picker hrs_picker = new Picker
            {
                Title = "00  ",
                

            };
            Picker min_picker = new Picker
            {
                Title = "00  ",

            };
            Label spli_time = new Label
            {
                Text = ":",
                VerticalOptions = LayoutOptions.Center
            };
            List<string> hrs = new List<string> { };

            for (int i = 0; i < 24; i++)
            {

               
                if (i < 10)
                {
                    hrs_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    hrs_picker.Items.Add(i.ToString());

                }


            }
            List<string> mins = new List<string> { };

            for (int i = 0; i < 60; i = i + 15)
            {
                if (i < 10)
                {
                    min_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    min_picker.Items.Add(i.ToString());

                }


            }

            picker = new Picker
            {
                Title = "Choose Project",

                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            picker.IsEnabled = false;
            picker.Items.Add("Choose Project");
            LoadProject();


            task = new Picker
            {
               
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            task.IsEnabled = false;

            //To display the picker value from JSon
            //picker.ItemDisplayBinding = new Binding(Constants.project_name);
            picker.SelectedIndexChanged += (sender, args) => {
                string projectselected = picker.Items[picker.SelectedIndex];

          

                if (projectselected.Equals("Choose Project"))
                {
                   // DisplayAlert("Oh Snap!", "Please select the project", "ok");
                    task.IsEnabled = false;
                    task.Title = null;
                }

                else
                {
                    LoadTask(projectselected);
                   
                }






            };

         
            task.ItemDisplayBinding = new Binding(Constants.task_name);

            var summit = new Button
            {
                Text = "Submit",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,

                BorderRadius = 20,
                HorizontalOptions = LayoutOptions.Center

            };

            var save = new Button
            {
                Text = "  Save  ",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,
                BorderRadius = 20,
            };
           

            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeeid", (sender, arg) =>
            //{
            //    setempid = arg;

            //    // stime.Format = arg;
            //});

            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeename", (sender, arg) =>
            //{

            //    setempname = arg;

            //});
            save.Clicked += async (object sender, EventArgs e) =>
            {
                bool getcheck = await check_connection();
                if (getcheck == true)
                {

                    if (picker.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the Project", "Ok");
                    }
                    else if (picker.SelectedIndex == 0)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the Project", "Ok");
                    }
                    else
                    {
                        if (task.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the Task", "Ok");
                        }
                        else if (hrs_picker.SelectedIndex == -1 && min_picker.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the Time", "Ok");

                        }
                        else
                        {

                            try
                            {
                                var client = new HttpClient();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                project_select = picker.Items[picker.SelectedIndex];
                                task_select = task.Items[task.SelectedIndex];
                                date = date_picker.Date.ToString(Constants.dateconvert);
                                //await DisplayAlert("Success", date, "okay");

                                if (hrs_picker.SelectedIndex == -1)
                                {
                                    hrs_value = "00";
                                }
                                else
                                {

                                    hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                                }


                                if (min_picker.SelectedIndex == -1)
                                {
                                    min_value = "00";
                                }
                                else
                                {
                                    min_value = min_picker.Items[min_picker.SelectedIndex];
                                }

                                time = hrs_value + ":" + min_value;
                                seconds_covert = TimeSpan.Parse(time).TotalSeconds.ToString();
                                desc = description.Text;
                                if (seconds_covert == "0")
                                {
                                    await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the Minutes", "Ok");
                                }
                                else
                                {
                                    save.IsEnabled = false;
                                    summit.IsEnabled = false;
                                    if (string.IsNullOrEmpty(setempid))
                                    {
                                        setempid = Helpers.Settings.Displayemployeeid;
                                    }
                                    if (string.IsNullOrEmpty(setcompid))
                                    {
                                        setcompid = Helpers.Settings.Displaycompanyid;
                                    }
                                    var postData = new List<KeyValuePair<string, string>>();
                                    postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                                    postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                                    postData.Add(new KeyValuePair<string, string>(Constants.project_name, project_select));
                                    postData.Add(new KeyValuePair<string, string>(Constants.task_name, task_select));
                                    postData.Add(new KeyValuePair<string, string>(Constants.hours, seconds_covert));
                                    postData.Add(new KeyValuePair<string, string>(Constants.date, date));
                                    postData.Add(new KeyValuePair<string, string>(Constants.approve_status, "Saved"));
                                    postData.Add(new KeyValuePair<string, string>(Constants.task_description, desc));
                                    var content = new FormUrlEncodedContent(postData);
                                    var response = await client.PostAsync(Constants.save_timesheet, content);
                                    //to take the value directly without the array

                                    string Jsonvalue = response.Content.ReadAsStringAsync().Result;
                                    //string JsonResult = response.Content.ReadAsStringAsync().Result;

                                    if (Jsonvalue == "Your Timesheet Saved Successfully")
                                    {

                                        getcurrentdate();
                                        await DisplayAlert("Success!", "Thank you!! " + Jsonvalue, "Ok");
                                        ent.LoadTimesheet();
                                    }
                                    else
                                    {
                                        getcurrentdate();
                                        await DisplayAlert("Oh Snap!", "Sorry, " + Jsonvalue, "Ok");
                                    }
                                    date_picker = new DatePicker()
                                    {
                                        Format = dateformat,
                                        HorizontalOptions = LayoutOptions.End


                                    };
                                    //  date_picker.Date = today_picker;
                                    //date_picker.Date = DateTime.Today;
                                    // 

                                    //to make the index to old format



                                    save.IsEnabled = true;
                                    summit.IsEnabled = true;
                                    picker.SelectedIndex = 0;

                                    hrs_picker.SelectedIndex = -1;
                                    min_picker.SelectedIndex = -1;
                                    description.Text = null;
                                }
                            }
                            catch (Exception ex) { }



                        }//-
                    }
                    //--
                }else
                {
                    await DisplayAlert("Oh Snap!", "Save the Timesheet after you Connect to Internet", "Ok");
                }
            };
            summit.Clicked += async (object sender, EventArgs e) =>
            {
                bool getcheck = await check_connection();
                if (getcheck == true)
                {

                    string time_enter = stime.Text;
                    if (picker.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the Project", "Ok");
                    }
                    else if (picker.SelectedIndex == 0)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the Project", "Ok");
                    }
                    else
                    {
                        if (task.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the Task", "Ok");
                        }
                        else if (hrs_picker.SelectedIndex == -1 && min_picker.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the Time", "Ok");

                        }
                        else
                        {

                            try
                            {

                                var client = new HttpClient();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                string project_select = picker.Items[picker.SelectedIndex];
                                string task_select = task.Items[task.SelectedIndex];
                                string date = date_picker.Date.ToString(Constants.dateconvert);
                                //await DisplayAlert("Success", date, "okay");
                                //hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                                if (hrs_picker.SelectedIndex == -1)
                                {
                                    hrs_value = "00";
                                }
                                else
                                {

                                    hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                                }

                                if (min_picker.SelectedIndex == -1)
                                {
                                    min_value = "00";
                                }
                                else
                                {
                                    min_value = min_picker.Items[min_picker.SelectedIndex];
                                }
                                time = hrs_value + ":" + min_value;
                                seconds_covert = TimeSpan.Parse(time).TotalSeconds.ToString();
                                desc = description.Text;
                                if (seconds_covert == "0")
                                {
                                    await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the Minutes", "Ok");
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(setempid))
                                    {
                                        setempid = Helpers.Settings.Displayemployeeid;
                                    }
                                    if (string.IsNullOrEmpty(setcompid))
                                    {
                                        setcompid = Helpers.Settings.Displaycompanyid;
                                    }


                                    save.IsEnabled = false;
                                    summit.IsEnabled = false;
                                    var postData = new List<KeyValuePair<string, string>>();
                                    postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                                    postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
                                    postData.Add(new KeyValuePair<string, string>(Constants.project_name, project_select));
                                    postData.Add(new KeyValuePair<string, string>(Constants.task_name, task_select));
                                    postData.Add(new KeyValuePair<string, string>(Constants.hours, seconds_covert));
                                    postData.Add(new KeyValuePair<string, string>(Constants.date, date));
                                    postData.Add(new KeyValuePair<string, string>(Constants.approve_status, "Submitted"));
                                    postData.Add(new KeyValuePair<string, string>(Constants.task_description, desc));
                                    var content = new System.Net.Http.FormUrlEncodedContent(postData);
                                    var response = await client.PostAsync(Constants.submit_timesheet, content);
                                    //to take the value directly without the array
                                    string Jsonvalue = response.Content.ReadAsStringAsync().Result;
                                    //string JsonResult = response.Content.ReadAsStringAsync().Result;
                                    if (Jsonvalue == "Your Timesheet Submitted Successfully")
                                    {

                                        getcurrentdate();
                                        await DisplayAlert("Success!", "Thank you!! " + Jsonvalue, "Ok");
                                        ent.LoadTimesheet();
                                    }
                                    else
                                    {
                                        getcurrentdate();
                                        await DisplayAlert("Oh Snap!", "Sorry, " + Jsonvalue, "Ok");
                                    }
                                    //date_picker = new DatePicker()
                                    //{
                                    //    Format = dateformat,
                                    //    HorizontalOptions = LayoutOptions.End


                                    //};

                                    // getcurrentdate();
                                    //  date_picker.Format = dateformat;
                                    //  date_picker.Date = DateTime.Today;
                                    //   date_picker.Date = today_picker;
                                    //to make the index to old format


                                    save.IsEnabled = true;
                                    summit.IsEnabled = true;
                                    picker.SelectedIndex = 0;
                                    hrs_picker.SelectedIndex = -1;
                                    min_picker.SelectedIndex = -1;
                                    description.Text = null;

                                }

                                //to take the value from the array it will be like the below lines
                                // string result = JsonResult.Trim(']').Trim('[');
                                //  var rootobject = JsonConvert.DeserializeObject<Person>(result.ToString());
                                //   var Status = rootobject.status;

                            }
                            catch (Exception ex) { }
                        }
                    }
                    //-
                    //---
                }
                else
                {
                    await DisplayAlert("Oh Snap!", "Submit the Timesheet after you Connect to Internet", "Ok");
                }
            };
            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Children = {
               new StackLayout
            {

                Padding = new Thickness(0,7,0,0),
              Spacing=3,
                Children = {
                new StackLayout {
                     Padding=new Thickness(20,3,20,0),

                    Children= {
                        //Setting the Frame
                new Frame {
                  BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                   Spacing=1,
                Children=
                    {
                         header,picker,header2,task
                    }
                }

                }
                    }
                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),
                    Children=
                    {
                        new Frame
                        {
                OutlineColor=Color.White,
                    HasShadow=true,
                  BackgroundColor=Color.FromHex("FFFFFF"),
                  Content=new StackLayout
                  {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                            Children=
                      {
                            description
                      }
                  }
                        }
                    }

                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),

                    Children= {
                   new Frame
                {
                 OutlineColor=Color.White,
                    HasShadow=true,

                  BackgroundColor=Color.FromHex("FFFFFF"),
                     Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                Children=
                    {
                             new StackLayout {
                                 Orientation=StackOrientation.Horizontal,

                                 Children= {

                              curdatetxt,date_picker


                                 }
                             },



                                 header3,

                                 new StackLayout
                                 {
                                     Orientation=StackOrientation.Horizontal,
                                     Padding=new Thickness(150,0,0,0),
                                     Children=
                                     {
                                           hrs_picker,spli_time,min_picker
                                     }
                                 },




                    }
                                  }
                }
                       }

                },
               

                       new StackLayout {
                  Orientation=StackOrientation.Horizontal,
                  HorizontalOptions=LayoutOptions.CenterAndExpand,
                Padding=new Thickness(0,10,0,0),
                  Children=
                    {
                       save,summit
                    }
                },
                   }
            }
        }
            };
        }
       

        private async void LoadTask(string projectselected)
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
                postData.Add(new KeyValuePair<string, string>(Constants.project_name, projectselected));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                var content = new System.Net.Http.FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.task, content);
                //to take the value directly without the array 
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                if (JsonResult.Equals("No"))
                {
                    task.Title = "Choose Task";
                    task.IsEnabled = false;
                }
                else
                {
                    var Items = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult);
                task.ItemsSource = Items;
                task.Title = "Choose Task";
                task.IsEnabled = true;
                }
            }
            else
            {
                await DisplayAlert("Oh Snap!", "Task won't be loaded until you Connect to Internet", "Ok");
            }
        }

        IPlatformElementConfiguration<T, Frame> IElementConfiguration<Frame>.On<T>()
        {
            throw new NotImplementedException();
        }

        public async void LoadProject()
        {
            bool getcheck = await check_connection();
            if (getcheck == true)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
                //postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.project, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                if (JsonResult.Equals("No"))
                {
                    picker.IsEnabled = false;
                }
                else
                {
                    string result = JsonResult.Trim(']').Trim('[');
                    string[] project_split = result.Split(',');
                    for (int i = 0; i < project_split.Length; i++)
                    {
                        string a = project_split[i];
                        string b = a.Trim('"').Trim('"');
                        picker.Items.Add(b);
                    }
                    picker.IsEnabled = true;
                }
            }
            else {
                await DisplayAlert("Oh Snap!", "Project won't be loaded until you Connect to Internet", "Ok");
            }
        }
        // get data used method
        public async void getdate()
        {
            var content = "";
            HttpClient client = new HttpClient();
           
            var RestURL = Constants.getting_Week_dates;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<LoadDate>(result.ToString());
            date_picker.MaximumDate = rootobject.Next;
            date_picker.MinimumDate = rootobject.Previous;
            // date_picker.Date = rootobject.current;
        }
        public async void getcurrentdate()
        {
            var content = "";
            HttpClient client = new HttpClient();

            var RestURL = Constants.current_date;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Loadcurrentdate>(result.ToString());
  
            date_picker.Date = rootobject.Current;
        }
        protected override bool OnBackButtonPressed()
        {
            var locator = CrossGeolocator.Current;
            //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


            bool getval = Helpers.Settings.DisplaySwitchstatus;
            if (locator.IsGeolocationEnabled && getval == true)
            {
                if (Device.OS == TargetPlatform.Android)
                {
                    DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                }
                Helpers.Settings.DisplayExitstatus = false;
                //return Helpers.Settings.DisplayExitstatus;
            }
            else
            {

                Helpers.Settings.DisplayExitstatus = true;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                    if (await result)
                    {
                        Helpers.Settings.DisplayLoginstatus = true;
                        await Navigation.PushAsync(new Report_injury());
                    }
                    else
                    {
                        string empname;
                        if (App.Current.Properties.ContainsKey("employeename"))
                        {
                            empname = (string)App.Current.Properties["employeename"];
                            Helpers.Settings.DisplayLoginstatus = true;
                            var a = Helpers.Settings.DisplayLoginstatus;
                            // await  DisplayAlert("Empname and status", empname+a, "ok");
                        }



                        if (Device.OS == TargetPlatform.Android)

                            DependencyService.Get<IAndroidMethods>().CloseApp();

                        //    // base.OnBackButtonPressed();
                        //
                    }

                });
                return Helpers.Settings.DisplayExitstatus;
            }


            return true;




        }
        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No Internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}