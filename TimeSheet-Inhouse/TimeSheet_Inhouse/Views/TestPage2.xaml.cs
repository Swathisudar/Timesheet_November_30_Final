﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestPage2 : ContentPage
    {
        public ListView listview;
        class Person
        {
            public Person(string productname, string categories, string latitude, string price)
            {
                this.productname = productname;
                this.categories = categories;
                this.latitude = latitude;
                this.price = price;
            }
            public string productname { private set; get; }

            public string categories { private set; get; }

            public string latitude
            {

                private set; get;
            }
            public string price { private set; get; }
        };
        public TestPage2()
        {
            InitializeComponent();
            this.Title = "View the Timesheet";
            Label header = new Label
            {
                Text = "View Timesheet for a week",
                FontSize = 20,
                TextColor=Color.Black,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Center
            };

            // Define some data.
            //List<Person> people = new List<Person>
            //{
            //     new Person("IFMR", "UI/UX","20-2-1976","10 hrs"),
            //    new Person("JC","Technical Design","21-2-1976","7 hrs"),
            //    new Person("MBox", "Requirement Analysis","22-2-1976","9 hrs"),
            //    new Person("Timesheet", "Core Development","23-2-1976","12 hrs"),
            //    new Person("IFMR", "UI/UX","24-2-1976","10 hrs"),
            //    new Person("JC","Technical Design","25-2-1975","7 hrs"),
            //    new Person("MBox", "Unit Testing", "28-2-1976","9 hrs"),
            //    new Person("Timesheet", "Core Development","29-2-1975","12 hrs")


            //};
            
            LoadData();
         listview = new ListView {
               
                ItemTemplate = new DataTemplate(() =>
                {
                    Label projectname = new Label() {
                        Font = Font.SystemFontOfSize(NamedSize.Medium)
              .WithAttributes(FontAttributes.Bold),
                        TextColor=Color.Black,
                    };


                    projectname.SetBinding(Label.TextProperty,"productname");
                    Label Taskname = new Label();
                    Taskname.SetBinding(Label.TextProperty, "categories");
                    Label projectdate = new Label();
                    projectdate.SetBinding(Label.TextProperty, "latitude");
                    Label totalhours = new Label()
                    {
                        Font = Font.SystemFontOfSize(NamedSize.Medium)
              .WithAttributes(FontAttributes.Bold),
                    };
                    totalhours.SetBinding(Label.TextProperty, "price");
                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                           
                            Orientation = StackOrientation.Horizontal,
                            Spacing = 2,
                            Children = {
                                new StackLayout {
                                     Padding= new Thickness(20,5,0,5),
                                    HorizontalOptions=LayoutOptions.StartAndExpand,
                                    Orientation=StackOrientation.Vertical,
                                    Spacing=2,
                                    Children= {
                                        projectname
                                        ,
                                         Taskname,
                                        projectdate,
                                    }
                              
                                },
                            
                            new StackLayout
                             {
                                Padding=new Thickness(0,25,30,5),
                                 Orientation=StackOrientation.Vertical,
                                 HorizontalOptions=LayoutOptions.EndAndExpand,
                                 Spacing=2,
                                 Children={
                                        
                                        totalhours
                                 }

                         }

                        }
                        }
                        };
                

                })

            };
            listview.ItemSelected += OnSelection;
            listview.RowHeight = 70;
            listview.SeparatorColor = Color.LightBlue;
            this.Content = new StackLayout {
              
                Children =
                {
                   
                   header,
                    listview
                }
            };
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            //if (e.SelectedItem == null)
            //{
            //    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            //}
            //var value = e.SelectedItem.ToString();
            //DisplayAlert("Item Selected", value, "Ok");
        }
        public async void LoadData()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/laravel-test/public/product_get";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<Person>>(content);
            listview.ItemsSource = Items;
        }
    }
}
