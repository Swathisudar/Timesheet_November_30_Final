﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;

namespace TimeSheet_Inhouse.ViewModel
{
   
    class ListContentViewModel
    {
        public List<ListContent> listcontent1 { get; set; }
        public ListContentViewModel()
        {
            listcontent1 = new ListContent().GetListContent();
        }
    }
}
