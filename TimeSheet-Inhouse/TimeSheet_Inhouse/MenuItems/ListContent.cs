﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
  
    class ListContent
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public List<ListContent> GetListContent()
        {
            List<ListContent> listcontent = new List<MenuItems.ListContent>()
            {
                new ListContent()
                {
                    Question="1. What is timesheet? ",

                    Answer="   A timesheet is a method of recording the amount of a worker's time spent on each job",
                },
                 new ListContent()
                {
                    Question="2. What is purpose of timesheet?",
                    Answer="   It is used for managing the time with specific project name an task by manuaally using a timer ",
                }, new ListContent()
                {
                    Question="3. How to enter the time sheet submit for approval?",
                    Answer="   -Select the project name \n   -Select the project task \n   -Enter the time by manually or use start timer and submit for approval"

                },
                  new ListContent()
                {
                    Question="4. What is the purpose of the timer?",
                    Answer="    A timer is used for specifying the working hours, by automatically instead of calculating the time manually",
                }, new ListContent()
                {
                    Question="5. How to enter the working hours on a weekly basis?",
                    Answer="   Select the date on the above tab and select the project name, task and enter the working hours manually",
                }, new ListContent()
                {
                    Question="6. How to edit the working hours?",
                    Answer="   Select the Item in the List and above will have three Options like Sumbit, Delete, Edit and then Click Edit on the above tab and enter the working hours manually and resubmit for approval.User can only use to modify Options when Timesheet have in Saved or Reject Other than that user can able to modify it",
                }, new ListContent()
                {
                    Question="7. About Back Button",
                    Answer="   When you are under tracking can't able to exit the application by Back Button Pressed only exit by Home button",
                },
            };
            return listcontent;

        }
    }
}
