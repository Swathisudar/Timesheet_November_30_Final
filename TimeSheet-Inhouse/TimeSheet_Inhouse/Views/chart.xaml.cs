﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class chart : ContentPage
    {
        public chart()
        {
            InitializeComponent();
            check_connection();
            this.Title = "Data";
            var vSampleData = new OxyExData();
            BindingContext = vSampleData;
        }
        private void ViewMethod(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DetailedviewChart());
        }
        protected override bool OnBackButtonPressed()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                //Cross Geolocator.Current is used for currently gps is ON state or OFF state 


                bool getval = Helpers.Settings.DisplaySwitchstatus;
                if (locator.IsGeolocationEnabled && getval == true)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().ShowSnackbar("Your are Track ON the Location. So, not allow to exiting via Back Button");
                    }
                    Helpers.Settings.DisplayExitstatus = false;
                  
                }
                else
                {

                    Helpers.Settings.DisplayExitstatus = true;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = DisplayAlert("Alert!", "Do you want to submit the Injury Report before exiting?", "Yes", "No");
                        if (await result)
                        {
                            Helpers.Settings.DisplayLoginstatus = true;
                            await Navigation.PushAsync(new Report_injury());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");
                        }
                        else
                        {
                            string empname;
                            if (App.Current.Properties.ContainsKey("employeename"))
                            {
                                empname = (string)App.Current.Properties["employeename"];
                                Helpers.Settings.DisplayLoginstatus = true;
                                var a = Helpers.Settings.DisplayLoginstatus;
                                // await  DisplayAlert("Empname and status", empname+a, "ok");
                            }

                            System.Diagnostics.Debug.WriteLine("************************************************");
                            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
                            System.Diagnostics.Debug.WriteLine("************************************************");

                            if (Device.OS == TargetPlatform.Android)

                                DependencyService.Get<IAndroidMethods>().CloseApp();

                            //    // base.OnBackButtonPressed();
                            //
                        }

                    });

                }
                base.OnBackButtonPressed();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("************************************************");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("************************************************");
            }
            System.Diagnostics.Debug.WriteLine("************************************************");
            System.Diagnostics.Debug.WriteLine(Helpers.Settings.DisplayLoginstatus.ToString());
            System.Diagnostics.Debug.WriteLine("************************************************");
            return true;
        }

        public async Task<bool> check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                await DisplayAlert("Whoops!", "No internet! Check your Connection", "Ok");
                check_connection();
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}