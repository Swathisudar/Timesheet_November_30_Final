﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TimeSheet_Inhouse
{
    class UnderlineEffect : RoutingEffect
    {
        public const string EffectNamespace = "Example";

        public UnderlineEffect() : base("MyCompany.FocusEffect")
        {
        }
    }
}
//$"{EffectNamespace}.{nameof(UnderlineEffect)}