﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {

        class Person
        {
            public Person(string productname, string categories, string latitude, string price)
            {
                this.productname = productname;
                this.categories = categories;
                this.latitude = latitude;
                this.price = price;
            }
            public string productname { private set; get; }

            public string categories { private set; get; }

            public string latitude
            {

                private set; get;
            }
            public string price { private set; get; }
        };

        public TabbedPage1()
        {
           
            LoadData();


            ItemTemplate = new DataTemplate(() =>
            {
          //      Label projectname = new Label()
          //      {
          //          Font = Font.SystemFontOfSize(NamedSize.Medium)
          //.WithAttributes(FontAttributes.Bold),
          //          TextColor = Color.Black,
          //      };

                Picker projectname = new Picker {
                    TextColor=Color.Black
                };
                projectname.ItemDisplayBinding = new Binding("productname");
                Label Taskname = new Label();
                Taskname.SetBinding(Label.TextProperty, "categories");
                Label projectdate = new Label();
                projectdate.SetBinding(Label.TextProperty, "latitude");
                Label totalhours = new Label()
                {
                    Font = Font.SystemFontOfSize(NamedSize.Medium)
          .WithAttributes(FontAttributes.Bold),
                };
                totalhours.SetBinding(Label.TextProperty, "price");
                var contentpage = new ContentPage
                {

                    Content = new StackLayout
                    {

                        Orientation = StackOrientation.Horizontal,
                        Spacing = 2,
                        Children = {
                         
                     new StackLayout {
                                     Padding= new Thickness(20,5,0,5),
                                    HorizontalOptions=LayoutOptions.StartAndExpand,
                                    Orientation=StackOrientation.Vertical,
                                    Spacing=2,
                                    Children= {
                                        projectname
                                        ,
                                         Taskname,
                                        projectdate,
                                    }

                                } ,

                       new StackLayout
                             {
                                Padding=new Thickness(0,25,30,5),
                                 Orientation=StackOrientation.Vertical,
                                 HorizontalOptions=LayoutOptions.EndAndExpand,
                                 Spacing=2,
                                 Children={

                                        totalhours
                                 }

                         }


                    }
                    }
                };
                contentpage.SetBinding(TitleProperty, "productname");
                return contentpage;

            });

            
        }

        private async void LoadData()
        {
            var content = "";
            HttpClient client = new HttpClient();
            var RestURL = "http://vps4.techiva.com/projects/laravel-test/public/product_get";
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = await response.Content.ReadAsStringAsync();
            var Items = JsonConvert.DeserializeObject<List<Person>>(content);
      this.ItemsSource = Items;
        }
    }
}